"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

var winuser = require('easywin').winuser;
var Instrumentation_1 = require("./src/Instrumentation");
var Form_1 = require("./src/Form");
var Image = require("./src/Image");
var Util = require('./src/Util');
var readline = require('readline-sync');
var fs = require("fs");

// Specify the input url here
var inputURL = "https://en.wikipedia.org/wiki/Special:WhatLinksHere/Main_Page";
var browser = new Instrumentation_1.Browser("chrome", true, true);
var associations = [];
var augmentations = [];
var solutions = [];
try {
    var url = inputURL;
    browser.moveWindowOffscreen();
    browser.goto(url);
    browser.fitWindowToContent__2();
    var forms = (0, Form_1.extractFormsObjects)(browser);
    if (forms.length === 0) {
        console.log("page has no forms. exiting.");
        process.exit();
    }
    else {
        var primaryLabels = (0, Form_1.solvePrimaryLabeling)(forms);
        var secondaryLabels = new Map();
        var programOutputs = [];
        if (primaryLabels) {
            programOutputs.push(JSON.parse(fs.readFileSync("primarySolution.json", { encoding: "utf8" })));
            fs.unlinkSync("primarySolution.json");
            try {
                secondaryLabels = (0, Form_1.solveSecondaryLabeling)(forms.flat(), primaryLabels);
                programOutputs.push(JSON.parse(fs.readFileSync("secondarySolution.json", { encoding: "utf8" })));
                fs.unlinkSync("secondarySolution.json");
            }
            catch (err) { }
        }
        else {
            console.log("subject form has no fields. exiting.");
            process.exit();
        }
        solutions.push({ subject: url, solutions: programOutputs });
        associations.push({ subject: url, associations: (0, Form_1.reportAssociations)(primaryLabels, secondaryLabels) });
        // console.log(`\n ${Math.round(tf-t0)} milliseconds`)
        // console.log(`${numFields}\t${numTexts}\t${numFandT}\t${numDOMEls}\t${runtime}`);
        // runtimeData += `${numFields}\t${numTexts}\t${numFandT}\t${numDOMEls}\t${runtime}\n`;
        //// Baseline
        // let labels = randomLabelingBaseline(forms, ss);
        var augmentation = (0, Form_1.applyRepairMarkup)(browser, primaryLabels, secondaryLabels);
        augmentations.push({ subject: url, augmentation: augmentation });
    }
}
catch (err) {
    console.log("" + err);
    process.exit();
}

// Comment / Uncomment to generate desired subset of outputs
fs.writeFileSync("associations.json", JSON.stringify(associations, null, 2));
// fs.writeFileSync("augmentations.json", JSON.stringify(augmentations, null, 2));
// fs.writeFileSync("solutions.json", JSON.stringify(solutions, null, 2));
console.log("\n\nFinished\n. quitting ...");
browser.quit();
