"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.Win32 = exports.MathOps = exports.runShellCommandAsync = exports.runShellCommand = exports.writeTempFile = exports.getHashOf = exports.executeSync = exports.getContentFromURL = void 0;

var crypto = require("crypto");
var fs = require("fs");
var child_process = require("child_process");
var deasync_1 = __importDefault(require("deasync"));
var https_1 = __importDefault(require("https"));
var xxhash_1 = __importDefault(require("xxhash"));
var win32_getWindowText = require("get-window-by-name").getWindowText;

function getContentFromURL(url) {
    var content = undefined;
    var exception = undefined;
    https_1.default.get(url, function (resp) {
        var data = '';
        resp.on('data', function (chunk) { return data += chunk; });
        resp.on('end', function () { return content = data; });
    }).on("error", function (err) { return exception = new Error(err.message); });
    deasync_1.default.loopWhile(function () { return content === undefined && exception === undefined; });
    if (exception)
        throw exception;
    if (content)
        return content;
}
exports.getContentFromURL = getContentFromURL;

function executeSync(thenable, timeoutInSeconds) {
    if (timeoutInSeconds === void 0) { timeoutInSeconds = Infinity; }
    var t0 = Date.now();
    var result = undefined, resultSet = false, failure = undefined, failureSet = false;
    thenable.then(function (resolved) {
        result = resolved;
        resultSet = true;
    }, function (rejected) {
        failure = rejected;
        failureSet = true;
    });
    deasync_1.default.loopWhile(function () {
        if ((Date.now() - t0) / 1000 > timeoutInSeconds) {
            failureSet = true;
            failure = "timeout while executing: " + thenable.toString();
            return false;
        }
        return !resultSet && !failureSet;
    });
    if (resultSet) {
        return result;
    }
    if (failureSet) {
        throw new Error(failure);
    }
}
exports.executeSync = executeSync;

function getHashOf(str) {
    return xxhash_1.default.hash(Buffer.from(str), 0xDEEDCAFE);
}
exports.getHashOf = getHashOf;

function writeTempFile(data, fileName, extension) {
    if (fileName === void 0) { fileName = null; }
    if (extension === void 0) { extension = ".png"; }
    fileName = fileName ? fileName : crypto.randomBytes(16).toString("hex");
    var path;
    if (process.platform === "darwin") {
        path = "/tmp/" + fileName + extension;
    }
    else if (process.platform === "win32") {
        path = "c:\\tmp\\" + fileName + extension;
    }
    else {
        throw Error("unsupported OS.");
    }
    fs.writeFileSync(path, data);
    return path;
}
exports.writeTempFile = writeTempFile;

function runShellCommand(command) {
    child_process.execSync(command);
}
exports.runShellCommand = runShellCommand;

function runShellCommandAsync(command) {
    child_process.exec(command);
}
exports.runShellCommandAsync = runShellCommandAsync;

var MathOps;
(function (MathOps) {
    function createMatrix(numRows, numCols, initialValue) {
        if (initialValue === void 0) { initialValue = 0; }
        var mat = Array(numRows).fill(0).map(function (x) { return Array(numCols).fill(initialValue); });
        return mat;
    }
    MathOps.createMatrix = createMatrix;

    function mode(arr) {
        return arr.sort(function (a, b) {
            return arr.filter(function (v) { return v === a; }).length
                - arr.filter(function (v) { return v === b; }).length;
        }).pop();
    }
    MathOps.mode = mode;

    function median(values) {
        values.sort(function (a, b) { return a - b; });
        var half = Math.floor(values.length / 2);
        if (values.length % 2)
            return values[half];
        else
            return (values[half - 1] + values[half]) / 2.0;
    }
    MathOps.median = median;
})(MathOps = exports.MathOps || (exports.MathOps = {}));

var Win32;
(function (Win32) {
    function FindPIDsByWindowTitle(windowTitle, exeName) {
        var e_1, _a, e_2, _b;
        if (exeName === void 0) { exeName = null; }
        var procs = win32_getWindowText();
        var pids = [];
        if (exeName) {
            try {
                for (var procs_1 = __values(procs), procs_1_1 = procs_1.next(); !procs_1_1.done; procs_1_1 = procs_1.next()) {
                    var proc = procs_1_1.value;
                    if (proc.processName === exeName && proc.processTitle.includes(windowTitle)) {
                        pids.push(proc.processId);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (procs_1_1 && !procs_1_1.done && (_a = procs_1.return)) _a.call(procs_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            try {
                for (var procs_2 = __values(procs), procs_2_1 = procs_2.next(); !procs_2_1.done; procs_2_1 = procs_2.next()) {
                    var proc = procs_2_1.value;
                    if (proc.processTitle.includes(windowTitle)) {
                        pids.push(proc.processId);
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (procs_2_1 && !procs_2_1.done && (_b = procs_2.return)) _b.call(procs_2);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        return pids;
    }
    Win32.FindPIDsByWindowTitle = FindPIDsByWindowTitle;

})(Win32 = exports.Win32 || (exports.Win32 = {}));
