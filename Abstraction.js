"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};

var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};

var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.VisualGrouping = exports.VisualGroup = exports.VisualObject = exports.VisualObjectType = exports.XPath = void 0;

var Geometry_1 = require("./Geometry");
var Util_1 = require("./Util");
var immutable_1 = require("immutable");

var XPath = /** @class */ (function () {
    function XPath(xpath) {
        this.value = xpath;
        this.hash = (0, Util_1.getHashOf)(this.value);
    }

    XPath.prototype.isHigherThan = function (other) {
        if (other) {
            return this.depth < other.depth;
        }
        else {
            return false;
        }
    };

    XPath.prototype.toString = function () {
        return this.value;
    };

    Object.defineProperty(XPath.prototype, "depth", {
        get: function () {
            return this.value.match(/\//gm).length;
        },
        enumerable: false,
        configurable: true
    });

    Object.defineProperty(XPath.prototype, "parent", {
        get: function () {
            var lastSlashIndex = this.value.lastIndexOf("/");
            if (lastSlashIndex > 0) {
                return new XPath(this.value.substring(0, lastSlashIndex));
            }
            else {
                return new XPath("/");
            }
        },
        enumerable: false,
        configurable: true
    });

    XPath.prototype.isRoot = function () {
        return this.value === "/";
    };

    XPath.prototype.isParentOf = function (other) {
        return (other && other.parent) ? other.parent.equals(this) : false;
    };

    XPath.prototype.isGrandParentOf = function (other) {
        if (other) {
            var parent_1 = other.parent;
            while (!parent_1.isRoot()) {
                parent_1 = parent_1.parent;
                if (parent_1 && parent_1.equals(this)) {
                    return true;
                }
            }
        }
        return false;
    };

    XPath.getNearestCommonAncestor = function (xpathArray) {
        var xpaths = xpathArray.map(function (x) { return x.toString(); });
        var NCA = "";
        var _loop_1 = function (i) {
            var chars = xpaths.map(function (xpath) { return xpath[i]; });
            if (chars.every(function (c) { return c === chars[0]; })) {
                NCA += chars[0];
            }
            else {
                return "break";
            }
        };
        for (var i = 0; i < Infinity; i++) {
            var state_1 = _loop_1(i);
            if (state_1 === "break")
                break;
        }
        var endPos = NCA.lastIndexOf("/");
        NCA = NCA.substring(0, endPos);
        return new XPath(NCA);
    };

    XPath.prototype.isChildOf = function (other) {
        return other.isParentOf(this);
    };

    XPath.prototype.isGrandChildOf = function (other) {
        return other.isGrandParentOf(this);
    };

    XPath.prototype.isDescendantOf = function (other) {
        return this.isChildOf(other) || this.isGrandChildOf(other);
    };

    XPath.prototype.equals = function (other) {
        return other ? this.hash === other.hash : false;
    };

    XPath.prototype.hashCode = function () {
        return this.hash;
    };
    return XPath;
}());
exports.XPath = XPath;

var VisualObjectType;
(function (VisualObjectType) {
    VisualObjectType["text"] = "text";
    VisualObjectType["image"] = "image";
    VisualObjectType["input"] = "input";
})(VisualObjectType = exports.VisualObjectType || (exports.VisualObjectType = {}));

var VisualObject = /** @class */ (function () {
    function VisualObject(args) {
        this.parentGroups = (0, immutable_1.Set)();
        this.parentGrouping = (0, immutable_1.Set)();
        this.cssSelector = args.cssSelector;
        this.xpath = args.xpath;
        this.type = args.type;
        this.boundingBox = args.boundingBox;
        this.attributes = args.attributes;
        this.computedStyle = args.computedStyle;
    }

    VisualObject.prototype.hashCode = function () {
        return (0, Util_1.getHashOf)(this.toString());
    };

    VisualObject.prototype.toString = function () {
        return [this.cssSelector, this.xpath.toString(), this.type, this.boundingBox.toString()].join(",");
    };

    VisualObject.prototype.equals = function (other) {
        if (other) {
            return (this.cssSelector === other.cssSelector
                && this.xpath.equals(other.xpath)
                && this.type === other.type);
        }
        else {
            return false;
        }
    };

    VisualObject.prototype.getXPath = function () {
        return this.xpath;
    };

    VisualObject.prototype.setXPath = function (xpath) {
        this.xpath = xpath;
    };

    VisualObject.prototype.getBoundingBox = function () {
        return this.boundingBox;
    };

    VisualObject.prototype.setBoundingBox = function (box) {
        this.boundingBox = box;
    };

    VisualObject.prototype.getCSSSelector = function () {
        return this.cssSelector;
    };

    VisualObject.prototype.getAttribute = function (attributeName) {
        var e_1, _a;
        try {
            for (var _b = __values(this.attributes), _c = _b.next(); !_c.done; _c = _b.next()) {
                var item = _c.value;
                if (item.attribute === attributeName) {
                    return item.value;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return undefined;
    };

    VisualObject.prototype.setAttribute = function (attributeName, attributeValue) {
        this.attributes.push({ "attribute": attributeName, "value": attributeValue });
    };

    VisualObject.prototype.getTagName = function () {
        var xpathString = this.xpath.toString().toLowerCase();
        return xpathString.substr(xpathString.lastIndexOf("/")).replace(/[\d\W]/gi, "");
    };

    VisualObject.prototype.getType = function () {
        return this.type;
    };

    VisualObject.prototype.isField = function () {
        var tag = this.getTagName();
        if (tag === "select" || tag === "textarea") {
            return true;
        }
        if (tag === "input" && "hidden|submit".includes(this.getAttribute("type")) === false) {
            return true;
        }
        return false;
    };

    VisualObject.prototype.isOptionField = function () {
        return this.getTagName() === "input" && "checkbox|radio".includes(this.getAttribute("type"));
    };

    VisualObject.prototype.isTextEntryField = function () {
        var tag = this.getTagName();
        if (tag === "select" || tag === "textarea"
            || (tag === "input" && "date|email|number|number|password|search|tel|text|url".includes(this.getAttribute("type")))
            || (tag === "input" && this.getAttribute("type") === undefined)) {
            return true;
        }
        else {
            return false;
        }
    };

    VisualObject.prototype.isInput = function () {
        return this.type === VisualObjectType.input;
    };

    VisualObject.prototype.isText = function () {
        return this.type === VisualObjectType.text;
    };
    return VisualObject;
}());
exports.VisualObject = VisualObject;

var VisualGroup = /** @class */ (function () {
    function VisualGroup(rootXPath, objects) {
        this._objects = (0, immutable_1.Set)();
        this._parent = null;
        this._childGroups = (0, immutable_1.Set)();
        this._rootXPath = rootXPath;
        this._objects = objects;
    }

    VisualGroup.prototype.getObjects = function () {
        return this._objects.toArray();
    };

    VisualGroup.prototype.getBoundingBox = function () {
        return Geometry_1.Rectangle.fromMergingOf(this.getObjects().map(function (vo) { return vo.getBoundingBox(); }));
    };

    VisualGroup.prototype.getParent = function () {
        return this._parent;
    };

    VisualGroup.prototype.getChildGroups = function () {
        return this._childGroups.toArray();
    };

    VisualGroup.prototype.addChildGroup = function (group) {
        var objectsOfChild = group._objects;
        group._parent = this;
        this._childGroups = this._childGroups.add(group);
        this._objects = this._objects.union(objectsOfChild);
    };

    VisualGroup.prototype.isEquivalentTo = function (other) {
        return (this._objects.subtract(other._objects).size === 0
            && other._objects.subtract(this._objects).size === 0);
    };

    VisualGroup.prototype.toString = function () {
        return this._rootXPath.toString();
    };

    VisualGroup.prototype.hashCode = function () {
        return (0, Util_1.getHashOf)(this.toString());
    };

    VisualGroup.prototype.equals = function (other) {
        return this._rootXPath.equals(other._rootXPath) && this.isEquivalentTo(other);
    };

    VisualGroup.mergeObjectsWithRedundantXPaths = function (visualObjects) {
        var processedVisualObjects = (0, immutable_1.Set)(visualObjects);
        var xpathCounter = (0, immutable_1.Map)();
        visualObjects.toArray().map(function (vo) { return vo.getXPath(); }).forEach(function (xpath) {
            if (xpathCounter.has(xpath)) {
                var count = xpathCounter.get(xpath);
                xpathCounter = xpathCounter.set(xpath, count + 1);
            }
            else {
                xpathCounter = xpathCounter.set(xpath, 1);
            }
        });
        var repeatedXPaths = (0, immutable_1.Set)(xpathCounter.filter(function (count) { return count > 1; }).keys());
        repeatedXPaths.forEach(function (repeatedXPath) {
            var matchingVisualObjects = visualObjects.filter(function (vo) { return vo.getXPath().equals(repeatedXPath); });
            processedVisualObjects = processedVisualObjects.subtract(matchingVisualObjects);
            var sortedObjectsByArea = matchingVisualObjects.sort(function (voA, voB) {
                var areaA = voA.getBoundingBox().getArea();
                var areaB = voB.getBoundingBox().getArea();
                if (areaA < areaB) {
                    return 1;
                }
                if (areaA > areaB) {
                    return -1;
                }
                return 0;
            });
            var mergedXPath = repeatedXPath;
            var mergedCssSelector = (matchingVisualObjects.first()).getCSSSelector();
            var mergedBoundingBox = Geometry_1.Rectangle.fromMergingOf(matchingVisualObjects.map(function (vo) { return vo.getBoundingBox(); }).toArray());
            var mergedType = (sortedObjectsByArea.first()).getType();
            var mergedObject = new VisualObject({
                cssSelector: mergedCssSelector,
                xpath: mergedXPath,
                type: mergedType,
                boundingBox: mergedBoundingBox,
            });
            processedVisualObjects = processedVisualObjects.add(mergedObject);
        });
        return processedVisualObjects;
    };

    VisualGroup.buildRootFromBrowser = function (browser) {
        var visualObjects = VisualGroup.buildVisualObjectsFromBrowser(browser);
        // visualObjects.toArray().map(vo => vo.getXPath().toString()).forEach((val, index) => console.log(`${index}: ${val}`));
        // console.log("\n\n");
        visualObjects = VisualGroup.mergeObjectsWithRedundantXPaths(visualObjects);
        var xpathGroups = groupXPaths(visualObjects.map(function (vo) { return vo.getXPath(); }));
        var visualObjectGroups = xpathGroups.map(function (xpathSet) { return xpathSet.map(function (xpath) { return visualObjects.find(function (vo) { return vo.getXPath().equals(xpath); }); }); });
        var finalGroupings = (0, immutable_1.Set)();
        visualObjectGroups.forEach(function (objectSet, xpath) {
            var group = new VisualGroup(xpath, objectSet);
            finalGroupings = finalGroupings.add(group);
            objectSet.forEach(function (object) { return object.parentGroups = object.parentGroups.add(group); });
        });
        // build parent-child relations
        // let groupsSortedByIncreasingXPathDepth = finalGroupings.sortBy(group => group.rootXPath.depth).toArray();
        var rootGroup = finalGroupings.sortBy(function (group) { return group._objects.size; }).reverse().toArray()[0];
        // let groups = finalGroupings.delete(rootGroup).toArray(); 
        var groups = finalGroupings.toArray();
        for (var i = 0, N = groups.length; i < N; i++) {
            for (var j = 0; j < N; j++) {
                if (groups[i]._rootXPath.isChildOf(groups[j]._rootXPath)
                    || groups[i]._rootXPath.isGrandChildOf(groups[j]._rootXPath)) {
                    groups[j].addChildGroup(groups[i]);
                }
            }
        }
        for (var i = 0, N = groups.length; i < N; i++) {
            if (groups[i]._childGroups.size === 1) {
                var childGroup = groups[i]._childGroups.first();
                if (groups[i]._objects.subtract(childGroup._objects).size === 0) {
                    groups[i]._childGroups = groups[i]._childGroups.delete(childGroup);
                }
            }
        }
        // return root;
        return rootGroup;
        // return groups;
    };

    VisualGroup.buildVisualObjectsFromBrowser = function (browser) {
        browser.loadScriptFromFile("lib/OptimalSelect.js");
        browser.loadScriptFromFile("src/Harness.js");
        var ROIs = browser.executeScript("return __HARNESS__.getROIs()");
        var visualObjects = ROIs.map(function (roi) { return new VisualObject({
            cssSelector: roi.cssSelector,
            xpath: new XPath(roi.xpath),
            type: (function (roiType) {
                if (roiType.toLowerCase().includes("text"))
                    return VisualObjectType.text;
                if (roiType.toLowerCase().includes("image"))
                    return VisualObjectType.image;
                if (roiType.toLowerCase().includes("input"))
                    return VisualObjectType.input;
            })(roi.type),
            boundingBox: Geometry_1.Rectangle.fromDOMRect(roi)
        }); });
        return (0, immutable_1.Set)(visualObjects);
    };

    VisualGroup.findAllGroups = function (vObjects) {
        var vObjectsArray = vObjects.toArray();
        var xpathsOriginal = vObjectsArray.map(function (vo) { return vo.getXPath(); });
        var xpaths_iteration_i = new (Array.bind.apply(Array, __spreadArray([void 0], __read(xpathsOriginal), false)))();
        var xpathGroups = (0, immutable_1.Set)(xpaths_iteration_i);
        var visualGroups = [];
        var groupings = (0, immutable_1.Map)();
        while (xpathGroups.size > 1) {
            xpaths_iteration_i = xpaths_iteration_i.map(function (xpath) { return xpath.parent; });
            xpathGroups = (0, immutable_1.Set)(xpaths_iteration_i);
            if (xpathGroups.size < xpaths_iteration_i.length) {
                xpathGroups.forEach(function (xpathOfGroup) {
                    groupings = groupings.set(xpathOfGroup, (0, immutable_1.Set)());
                    xpathsOriginal.forEach(function (xpath, voIndex) {
                        if (xpath.isChildOf(xpathOfGroup)) {
                            var set_i = groupings.get(xpathOfGroup);
                            set_i = set_i.add(vObjectsArray[voIndex]);
                            groupings = groupings.set(xpathOfGroup, set_i);
                        }
                    });
                    // if (groupings.get(xpathOfGroup).size<=1) {
                    //     groupings = groupings.delete(xpathOfGroup);
                    // }
                });
            }
            else {
                continue;
            }
        }
        // Confirmed: each original object in <vObjects>
        // is added to an item in <groupings>
        Array.from(groupings.entries()).map(function (entry, index) {
            var _a;
            var rootXPathOfGroup, objects;
            _a = __read(entry, 2), rootXPathOfGroup = _a[0], objects = _a[1];
            var group = new VisualGroup(rootXPathOfGroup, objects);
            visualGroups.push(group);
        });
        for (var i = 0, N = visualGroups.length; i < N; i++) {
            for (var j = 0; j < N; j++) {
                if (visualGroups[i]._rootXPath.isParentOf(visualGroups[j]._rootXPath)
                // || visualGroups[i].rootXPath.isGrandParentOf(visualGroups[j].rootXPath)
                ) {
                    // visualGroups[j].updateParent(visualGroups[i]);
                    visualGroups[i].addChildGroup(visualGroups[j]);
                }
            }
        }
        return (0, immutable_1.Set)(visualGroups);
    };
    return VisualGroup;
}());
exports.VisualGroup = VisualGroup;

var VisualGrouping = /** @class */ (function () {
    function VisualGrouping() {
        this.objects = [];
        this.parent = null;
        this.childGroups = [];
    }

    VisualGrouping.loadDependencies = function (browser) {
        browser.loadScriptFromFile("lib/OptimalSelect.js");
        browser.loadScriptFromFile("src/Harness.js");
    };

    VisualGrouping.mergeObjectsWithRedundantXPaths = function (visualObjects) {
        var processedVisualObjects = (0, immutable_1.Set)(visualObjects);
        var xpathCounter = (0, immutable_1.Map)();
        visualObjects.map(function (vo) { return vo.getXPath(); }).forEach(function (xpath) {
            if (xpathCounter.has(xpath)) {
                var count = xpathCounter.get(xpath);
                xpathCounter = xpathCounter.set(xpath, count + 1);
            }
            else {
                xpathCounter = xpathCounter.set(xpath, 1);
            }
        });
        var repeatedXPaths = (0, immutable_1.Set)(xpathCounter.filter(function (count) { return count > 1; }).keys());
        repeatedXPaths.forEach(function (repeatedXPath) {
            var matchingVisualObjects = visualObjects.filter(function (vo) { return vo.getXPath().equals(repeatedXPath); });
            // let sortedObjectsByArea = matchingVisualObjects.sort((voA, voB) => {
            //     let areaA = voA.getBoundingBox().getArea();
            //     let areaB = voB.getBoundingBox().getArea();
            //     if (areaA < areaB) { return 1; }
            //     if (areaA > areaB) { return -1;}
            //     return 0;
            // });
            var mergedXPath = repeatedXPath;
            var mergedCssSelector = matchingVisualObjects[0].getCSSSelector();
            // let mergedBoundingBox = Rectangle.fromMergingOf(matchingVisualObjects.map(vo => vo.getBoundingBox()));
            var mergedBoundingBox = matchingVisualObjects[0].getBoundingBox();
            var mergedType = matchingVisualObjects[0].getType();
            var mergedAttributes = matchingVisualObjects[0].attributes;
            var mergedComputedStyle = matchingVisualObjects[0].computedStyle;
            var mergedVisualObject = new VisualObject({
                cssSelector: mergedCssSelector,
                xpath: mergedXPath,
                type: mergedType,
                boundingBox: mergedBoundingBox,
                attributes: mergedAttributes,
                computedStyle: mergedComputedStyle
            });
            processedVisualObjects = processedVisualObjects.subtract(matchingVisualObjects);
            processedVisualObjects = processedVisualObjects.add(mergedVisualObject);
        });
        return processedVisualObjects.toArray();
    };

    VisualGrouping.buildVisualObjectsFromBrowser = function (browser) {
        browser.loadScriptFromFile("lib/OptimalSelect.js");
        browser.loadScriptFromFile("src/Harness.js");
        var ROIs = browser.executeScript("return __HARNESS__.getROIs()");
        var visualObjects = ROIs.map(function (roi) {
            var object = new VisualObject({
                cssSelector: roi.cssSelector,
                xpath: new XPath(roi.xpath),
                type: (function (roiType) {
                    if (roiType.toLowerCase().includes("input"))
                        return VisualObjectType.input;
                    if (roiType.toLowerCase().includes("text"))
                        return VisualObjectType.text;
                    if (roiType.toLowerCase().includes("image"))
                        return VisualObjectType.image;
                })(roi.type),
                boundingBox: Geometry_1.Rectangle.fromDOMRect(roi),
                attributes: roi.attributes,
                computedStyle: roi.computedStyle,
            });
            var box = object.getBoundingBox();
            if (box.left < 0)
                box.left = 0;
            if (box.top < 0)
                box.top = 0;
            object.setBoundingBox(box);
            return object;
        });
        return VisualGrouping.mergeObjectsWithRedundantXPaths((0, immutable_1.Set)(visualObjects).toArray());
    };

    VisualGrouping.buildGroupsFromBrowser = function (browser) {
        var ss = browser.takeScreenshot(true);
        // let screenRect = new Rectangle(0, 0, ss.getWidth(), ss.getHeight())
        VisualGrouping.loadDependencies(browser);
        var visualObjects = VisualGrouping.buildVisualObjectsFromBrowser(browser);
        visualObjects = VisualGrouping.mergeObjectsWithRedundantXPaths(visualObjects);
        var rawGroups = browser.executeScript("return __HARNESS__.extractAllGroups();");
        var groups = rawGroups.map(function (group) {
            var groupObject = new VisualGrouping();
            groupObject.boundingBox = Geometry_1.Rectangle.fromDOMRect(group);
            groupObject.xpath = new XPath(group.rootXPath);
            groupObject.cssSelector = group.cssSelector;
            groupObject.parent = null;
            return groupObject;
        });
        groups.forEach(function (group) {
            var objectSet = (0, immutable_1.Set)();
            visualObjects.forEach(function (object) {
                if (group.boundingBox.contains(object.getBoundingBox())) {
                    objectSet = objectSet.add(object);
                    object.parentGrouping = object.parentGrouping.add(group);
                }
            });
            group.objects = objectSet.toArray();
        });
        groups = VisualGrouping.removeRedundancies(groups);
        groups.forEach(function (groupX) {
            groups.forEach(function (groupY) {
                var YinX = false, XinY = false;
                if (groupX === groupY)
                    return;
                if (groupX.boundingBox.contains(groupY.boundingBox)) {
                    groupX.childGroups.push(groupY);
                    groupY.parent = groupX;
                    YinX = true;
                }
                if (groupY.boundingBox.contains(groupX.boundingBox)) {
                    groupY.childGroups.push(groupX);
                    groupX.parent = groupY;
                    XinY = true;
                }
                if (XinY && YinX) {
                    console.error("both X and Y are parents of each other. Opening debugger ...");
                    debugger;
                }
            });
        });
        // keep root groups only
        groups = groups.filter(function (group) { return group.parent === null; });
        // let sum = 0;
        // groups.forEach((group, index) => {
        //     console.log(`${index}: ${group.objects.length} objects`);
        //     sum += group.objects.length;
        // })
        // console.log(`net objects count: ${sum}`);
        // debugger;
        return groups;
    };

    VisualGrouping.removeRedundancies = function (groups) {
        groups = groups.sort(function (groupA, groupB) { return groupA.objects.length - groupB.objects.length; });
        var removedRootGroup = groups.pop();
        var netObjectsCount = removedRootGroup.objects.length;
        groups = groups.filter(function (group) { return group.objects.length > 1 && group.boundingBox.getArea() > 100 && group.objects.length < netObjectsCount / 3; });
        var deletionSet = (0, immutable_1.Set)();
        for (var i = groups.length - 1; i >= 0; i--) {
            if (deletionSet.has(groups[i]))
                continue;
            var objects_i = (0, immutable_1.Set)(groups[i].objects);
            for (var j = groups.length - 1; j >= 0; j--) {
                if (i === j)
                    continue;
                if (deletionSet.has(groups[j]))
                    continue;
                var objects_j = (0, immutable_1.Set)(groups[j].objects);
                if (objects_i.subtract(objects_j).size === 0
                    && objects_j.subtract(objects_i).size === 0) {
                    if (groups[i].boundingBox.getArea() < groups[j].boundingBox.getArea()) {
                        deletionSet = deletionSet.add(groups[j]);
                    }
                    else {
                        deletionSet = deletionSet.add(groups[i]);
                    }
                }
            }
        }
        groups = (0, immutable_1.Set)(groups).subtract(deletionSet).toArray();
        groups = groups.map(function (group) {
            group.boundingBox = Geometry_1.Rectangle.fromMergingOf(group.objects.map(function (o) { return o.getBoundingBox(); })).expandBy(2);
            return group;
        });
        return groups;
    };
    return VisualGrouping;
}());
exports.VisualGrouping = VisualGrouping;

function groupXPaths(xpaths) {
    var inputXPaths = xpaths;
    var xpaths_iteration_i = inputXPaths.map(function (xpath) { return xpath.parent; });
    var groups = (0, immutable_1.Map)();
    var shouldStop = false;
    while (!shouldStop) {
        if (xpaths_iteration_i.size < inputXPaths.size) {
            var xpathGroups_iteration_i = (0, immutable_1.Set)(xpaths_iteration_i);
            xpathGroups_iteration_i.forEach(function (group) {
                if (shouldStop)
                    return;
                var children = (0, immutable_1.Set)();
                inputXPaths.forEach(function (xpath) {
                    if (xpath.isChildOf(group) || xpath.isGrandChildOf(group))
                        children = children.add(xpath);
                });
                if (children.size > 1)
                    groups = groups.set(group, children);
                if (children.size === inputXPaths.size) {
                    shouldStop = true;
                }
            });
        }
        xpaths_iteration_i = xpaths_iteration_i.map(function (xpath) { return xpath.parent; });
        if (xpaths_iteration_i.size <= 1) {
            shouldStop = true;
        }
    }
    return groups;
}
