"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.solveSecondaryLabeling = exports.applyRepairMarkup = exports.reportAssociations = exports.solvePrimaryLabeling = exports.randomLabelingBaseline = exports.extractFormsObjects = void 0;

var Abstraction_1 = require("./Abstraction");
var perf_hooks_1 = require("perf_hooks");
var Geometry_1 = require("./Geometry");
var Solver_1 = require("./Solver");
var ml_hclust_1 = require("ml-hclust");
var Util_1 = require("./Util");

function extractFormsObjects(browser) {
    var e_1, _a, e_2, _b;
    var allObjects = Abstraction_1.VisualGrouping.buildVisualObjectsFromBrowser(browser);
    var formElements = browser.executeScript("\n        return Array.from(document.querySelectorAll(\"form\")).map(e => __HARNESS__._getXpathOfElement(e));\n    ");
    var formObjectsList = [];
    var i = 0;
    try {
        for (var formElements_1 = __values(formElements), formElements_1_1 = formElements_1.next(); !formElements_1_1.done; formElements_1_1 = formElements_1.next()) {
            var form = formElements_1_1.value;
            var formXPath = new Abstraction_1.XPath(form);
            var formObjects = [];
            try {
                for (var allObjects_1 = (e_2 = void 0, __values(allObjects)), allObjects_1_1 = allObjects_1.next(); !allObjects_1_1.done; allObjects_1_1 = allObjects_1.next()) {
                    var object = allObjects_1_1.value;
                    if (object.getXPath().isDescendantOf(formXPath)) {
                        formObjects.push(object);
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (allObjects_1_1 && !allObjects_1_1.done && (_b = allObjects_1.return)) _b.call(allObjects_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
            if (formObjects.length > 0) {
                formObjectsList.push(formObjects);
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (formElements_1_1 && !formElements_1_1.done && (_a = formElements_1.return)) _a.call(formElements_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return formObjectsList;
}
exports.extractFormsObjects = extractFormsObjects;

function randomLabelingBaseline(forms, tempScreenshot) {
    var e_3, _a, e_4, _b;
    if (tempScreenshot === void 0) { tempScreenshot = null; }
    var flipCoin = function () { return Math.random() > 0.5; };
    var result = new Map();
    try {
        for (var forms_1 = __values(forms), forms_1_1 = forms_1.next(); !forms_1_1.done; forms_1_1 = forms_1.next()) {
            var form = forms_1_1.value;
            var fields = form.filter(function (o) { return o.isField(); });
            var texts = form.filter(function (o) { return o.isText(); });
            try {
                for (var fields_1 = (e_4 = void 0, __values(fields)), fields_1_1 = fields_1.next(); !fields_1_1.done; fields_1_1 = fields_1.next()) {
                    var objA = fields_1_1.value;
                    if (flipCoin()) {
                        var objB = texts[texts.length * Math.random() | 0];
                        result.set(objA, objB);
                    }
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (fields_1_1 && !fields_1_1.done && (_b = fields_1.return)) _b.call(fields_1);
                }
                finally { if (e_4) throw e_4.error; }
            }
        }
    }
    catch (e_3_1) { e_3 = { error: e_3_1 }; }
    finally {
        try {
            if (forms_1_1 && !forms_1_1.done && (_a = forms_1.return)) _a.call(forms_1);
        }
        finally { if (e_3) throw e_3.error; }
    }
    return result.size > 0 ? result : null;
}
exports.randomLabelingBaseline = randomLabelingBaseline;

var normalizeObjectPath = function (object) {
    if (object.getXPath().toString().endsWith("/#text")) {
        object.setXPath(object.getXPath().parent);
    }
    return object;
};

function solvePrimaryLabeling(forms) {
    var e_5, _a;
    var extractImplicitTextObjects = function (visualObjects) {
        var makeImplicitTextObjectForEntryField = function (object) {
            if (object.isTextEntryField()
                && object.getAttribute("data-cortex-implicit-text")
                && object.getAttribute("data-cortex-implicit-text").length > 0) {
                return new Abstraction_1.VisualObject({
                    cssSelector: object.getCSSSelector(),
                    xpath: new Abstraction_1.XPath(object.getXPath().toString() + "/#text"),
                    type: Abstraction_1.VisualObjectType.text,
                    boundingBox: object.getBoundingBox(),
                    attributes: object.attributes,
                    computedStyle: object.computedStyle
                });
            }
            else {
                return null;
            }
        };
        var newObjects = visualObjects.map(function (it) { return makeImplicitTextObjectForEntryField(it); }).filter(function (it) { return it !== null; });
        newObjects.forEach(function (it) { return it.setAttribute("is-implicit-text", true); });
        return newObjects;
    };
    // console.log(`\n -- Primary Labeling --`)
    var result = new Map();
    try {
        for (var forms_2 = __values(forms), forms_2_1 = forms_2.next(); !forms_2_1.done; forms_2_1 = forms_2.next()) {
            var form = forms_2_1.value;
            var fields = form.filter(function (o) { return o.isField(); });
            var texts = form.filter(function (o) { return o.isText(); }).concat(extractImplicitTextObjects(fields));
            if (fields.length > 0 && texts.length > 0) { // non-empty <form>s
                var program = new Solver_1.PrimarySolver(fields, texts, null);
                var labels = program.solve();
                labels.forEach(function (value, key) { return result.set(key, normalizeObjectPath(value)); });
            }
        }
    }
    catch (e_5_1) { e_5 = { error: e_5_1 }; }
    finally {
        try {
            if (forms_2_1 && !forms_2_1.done && (_a = forms_2.return)) _a.call(forms_2);
        }
        finally { if (e_5) throw e_5.error; }
    }
    return result.size > 0 ? result : null;
}
exports.solvePrimaryLabeling = solvePrimaryLabeling;

function reportAssociations(primary, secondary) {
    var output = [];
    primary.forEach(function (value, key) { return output.push([[value.getXPath().toString()], [key.getXPath().toString()]]); });
    secondary.forEach(function (value, key) { return output.push([[value.getXPath().toString()], key.map(function (o) { return o.getXPath().toString(); })]); });
    return output;
    // return JSON.stringify(output, null, 2);
}
exports.reportAssociations = reportAssociations;

function applyRepairMarkup(browser, primary, secondary) {
    var e_6, _a, e_7, _b;
    var repairScript = "\n    const __AxeFormRepair__ = {\n        getElementByXPath: (xpath) => {\n            try {\n                return document.evaluate(xpath, document, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null).singleNodeValue\n            } catch (err) {\n                return null\n            }\n        },\n    };\n    ";
    var UUID = function () { return ('000000000' + Math.random().toString(36).substr(2, 9)).slice(-9); };
    var db_allXpaths = [];
    if (primary) {
        try {
            for (var _c = __values(primary.keys()), _d = _c.next(); !_d.done; _d = _c.next()) {
                var field = _d.value;
                var label = primary.get(field);
                var fieldXPath = field.getXPath().toString();
                var labelID = void 0;
                if (label.getAttribute("id")) {
                    labelID = label.getAttribute("id");
                }
                else {
                    labelID = UUID();
                    var labelXPath = label.getXPath().toString();
                    repairScript += "\n                __AxeFormRepair__.getElementByXPath(\"" + labelXPath + "\").id = \"" + labelID + "\";\n                ";
                    db_allXpaths.push(labelXPath);
                }
                repairScript += "\n            __AxeFormRepair__.getElementByXPath(\"" + fieldXPath + "\").setAttribute(\"aria-labelledby\", \"" + labelID + "\");\n            ";
                db_allXpaths.push(fieldXPath);
            }
        }
        catch (e_6_1) { e_6 = { error: e_6_1 }; }
        finally {
            try {
                if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
            }
            finally { if (e_6) throw e_6.error; }
        }
    }
    if (secondary) {
        try {
            for (var _e = __values(secondary.keys()), _f = _e.next(); !_f.done; _f = _e.next()) {
                var fieldGroup = _f.value;
                var label = secondary.get(fieldGroup);
                var xpaths = fieldGroup.map(function (field) { return field.getXPath(); });
                var NCAXPath = Abstraction_1.XPath.getNearestCommonAncestor(xpaths).toString();
                var labelID = void 0;
                if (label.getAttribute("id")) {
                    labelID = label.getAttribute("id");
                }
                else {
                    labelID = UUID();
                    var labelXPath = label.getXPath().toString();
                    repairScript += "\n                __AxeFormRepair__.getElementByXPath(\"" + labelXPath + "\").id = \"" + labelID + "\";\n                ";
                    db_allXpaths.push(labelXPath);
                }
                repairScript += "\n            __AxeFormRepair__.getElementByXPath(\"" + NCAXPath + "\").setAttribute(\"role\", \"group\");\n            __AxeFormRepair__.getElementByXPath(\"" + NCAXPath + "\").setAttribute(\"aria-labelledby\", \"" + labelID + "\");\n            ";
                db_allXpaths.push(NCAXPath);
            }
        }
        catch (e_7_1) { e_7 = { error: e_7_1 }; }
        finally {
            try {
                if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
            }
            finally { if (e_7) throw e_7.error; }
        }
    }
    browser.executeScript("\n    try {\n        " + repairScript + "\n    } catch (exc) {}\n    ");
    return repairScript;
    // // // debug
    // db_allXpaths.forEach(xpath => {
    //     let found = browser.executeScript(`
    //     const __AxeFormRepair__ = {
    //         getElementByXPath: (xpath) => {
    //             try {
    //                 return document.evaluate(xpath, document, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null).singleNodeValue
    //             } catch (err) {
    //                 return null
    //             }
    //         },
    //     };
    //     return __AxeFormRepair__.getElementByXPath("${xpath}")!==null;
    //     `);
    //     console.log(`${xpath} \t => \t ${found}`)
    // })
}
exports.applyRepairMarkup = applyRepairMarkup;

function solveSecondaryLabeling(form, primaryLabeling) {
    var e_8, _a;
    if (primaryLabeling === void 0) { primaryLabeling = null; }
    var optionsFields = form.filter(function (item) { return item.getTagName() === "input" && "checkbox|radio".includes(item.getAttribute("type")); });
    var optionsGroups = optionsGrouper(optionsFields);
    var primaryLabeledOptions = Array.from(primaryLabeling.keys()).filter(function (field) { return field.getTagName() === "input" && "checkbox|radio".includes(field.getAttribute("type")); });
    // let primaryLabels = labeledFields.map(field => primaryLabeling.get(field));
    var primaryLabels = Array.from(primaryLabeling.values());
    // let unassignedTexts = form.filter(vo => vo.isText() && primaryLabels.includes(vo)===false);
    var unassignedTexts = [];
    for (var i = 0; i < form.length; i++) {
        if (!form[i].isText())
            continue;
        var matchFound = false;
        var rectA = form[i].getBoundingBox();
        for (var j = 0; j < primaryLabels.length; j++) {
            var rectB = primaryLabels[j].getBoundingBox();
            if (rectA.overlapsWith(rectB)) {
                matchFound = true;
                break;
            }
            else {
                continue;
            }
        }
        if (!matchFound)
            unassignedTexts.push(form[i]);
    }
    var secondaryGroupedFields = [];
    var virtualFieldMap = new Map();
    try {
        for (var optionsGroups_1 = __values(optionsGroups), optionsGroups_1_1 = optionsGroups_1.next(); !optionsGroups_1_1.done; optionsGroups_1_1 = optionsGroups_1.next()) {
            var group = optionsGroups_1_1.value;
            if (group.length <= 1)
                continue;
            var boundingBox = Geometry_1.Rectangle.fromMergingOf(group.map(function (o) { return o.getBoundingBox(); })).clone().expandBy(10);
            var virtualObject = new Abstraction_1.VisualObject({ type: Abstraction_1.VisualObjectType.input, boundingBox: boundingBox, xpath: new Abstraction_1.XPath("/input"),
                attributes: [{ attribute: "type", value: "radio" }] });
            secondaryGroupedFields.push(virtualObject);
            virtualFieldMap.set(virtualObject, group);
        }
    }
    catch (e_8_1) { e_8 = { error: e_8_1 }; }
    finally {
        try {
            if (optionsGroups_1_1 && !optionsGroups_1_1.done && (_a = optionsGroups_1.return)) _a.call(optionsGroups_1);
        }
        finally { if (e_8) throw e_8.error; }
    }
    // console.log(`\n -- Secondary Labeling --`);
    var result = new Map();
    var program = new Solver_1.SecondarySolver(secondaryGroupedFields, unassignedTexts, { optionsMax: false });
    var labels = program.solve();
    labels.forEach(function (value, key) { return result.set(virtualFieldMap.get(key), normalizeObjectPath(value)); });
    return result;
}
exports.solveSecondaryLabeling = solveSecondaryLabeling;

var optionsGrouper = function (fields) {
    var e_9, _a, e_10, _b;
    var N = fields.length, rectA, rectB;
    var matrix = Util_1.MathOps.createMatrix(N, N, 0);
    var t0, tf;
    t0 = perf_hooks_1.performance.now();
    for (var i = 0; i < N; i++) {
        matrix[i][i] = 0;
        rectA = fields[i].getBoundingBox();
        for (var j = i + 1; j < N; j++) {
            rectB = fields[j].getBoundingBox();
            matrix[i][j] = rectA.getMinimumDistanceTo(rectB);
            matrix[j][i] = matrix[i][j];
        }
    }
    var groups = [];
    if (N > 1) {
        var tree = (0, ml_hclust_1.agnes)(matrix, { method: 'single', isDistanceMatrix: true });
        var heights_1 = [];
        tree.traverse(function (node) { return heights_1.push(node.height); });
        tf = perf_hooks_1.performance.now();
        // console.log(" hclust runtime = " + (tf-t0).toFixed(1) + " milliseconds");
        heights_1 = heights_1.filter(function (h) { return h > 0; });
        var cutoff = Util_1.MathOps.median(heights_1) + 1;
        var clusters = tree.cut(cutoff);
        try {
            for (var clusters_1 = __values(clusters), clusters_1_1 = clusters_1.next(); !clusters_1_1.done; clusters_1_1 = clusters_1.next()) {
                var cluster = clusters_1_1.value;
                var indexes = cluster.indices();
                if (indexes.length === 1)
                    continue;
                var objects = indexes.map(function (i) { return fields[i]; });
                groups.push(objects);
            }
        }
        catch (e_9_1) { e_9 = { error: e_9_1 }; }
        finally {
            try {
                if (clusters_1_1 && !clusters_1_1.done && (_a = clusters_1.return)) _a.call(clusters_1);
            }
            finally { if (e_9) throw e_9.error; }
        }
        var allGroupedFields_1 = groups.flat();
        var loneFields = fields.filter(function (field) { return allGroupedFields_1.includes(field) === false; });
        try {
            for (var loneFields_1 = __values(loneFields), loneFields_1_1 = loneFields_1.next(); !loneFields_1_1.done; loneFields_1_1 = loneFields_1.next()) {
                var field = loneFields_1_1.value;
                groups.push([field]);
            }
        }
        catch (e_10_1) { e_10 = { error: e_10_1 }; }
        finally {
            try {
                if (loneFields_1_1 && !loneFields_1_1.done && (_b = loneFields_1.return)) _b.call(loneFields_1);
            }
            finally { if (e_10) throw e_10.error; }
        }
        return groups;
    }
    return [];
};
/*
// import { SpatialIndex, Rectangle } from "./Geometry";
import Flatbush from 'flatbush';
import { agnes } from 'ml-hclust';
import { performance } from 'perf_hooks';
import {inspect} from "util";
import {MathOps} from "./Util";

class ObjectsSpatialIndex {
    private objects: VisualObject[];
    private index: Flatbush.Flatbush;

    constructor(visualObjects: VisualObject[]) {
        this.objects = visualObjects;
        this.index = new Flatbush(visualObjects.length);
        for (let i=0; i<visualObjects.length; i++) {
            let rect = visualObjects[i].getBoundingBox();
            this.index.add(rect.left, rect.top, rect.right, rect.bottom);
        }
        this.index.finish();
    }

    getNeighbors(query: VisualObject, maxDistance: number = Infinity): VisualObject[] {
        let [x, y] = query.getBoundingBox().getCenter();
        return this.index.neighbors(x, y, this.objects.length, maxDistance).map(i => this.objects[i]);
    }
}

export function getRegions(browser: Browser): FormRegion[] {
    // let groups: VisualGrouping[] = VisualGrouping.buildGroupsFromBrowser(browser);
    let ss = browser.takeScreenshot(true);
    let ss2 = ss.clone();
    let allObjects: VisualObject[] = VisualGrouping.buildVisualObjectsFromBrowser(browser);
    // allObjects = VisualGrouping.mergeObjectsWithRedundantXPaths(allObjects);
    // let allInputObjects: VisualObject[] = groups.map(group => group.objects).flat().filter(o => o.isInput());
    let allInputObjects: VisualObject[] = allObjects.filter(o => o.isField());

    let countOfFormChildren = 0;
    for (let object of allInputObjects) {
        if (object.getXPath().toString().includes("form")) {
            countOfFormChildren++;
        }
    }

    console.log(`
    Found ${allInputObjects.length} field objects,
    ${countOfFormChildren} of which are children of a <form> element.
    `);

    allInputObjects.forEach(object => {
        console.log(`${object.getTagName()} type=${object.getAttribute("type")}  rect=${object.getBoundingBox().toString()}`)
    });
    ss2.show("input_objects_"+allInputObjects.length);

    let N = allInputObjects.length, rectA: Rectangle, rectB: Rectangle;
    let matrix: number[][] = Array(N).fill(0).map(x => Array(N).fill(0));

    for (let i=0; i<N; i++) {
        matrix[i][i] = 0;
        rectA = allInputObjects[i].getBoundingBox();
        for (let j=i+1; j<N; j++) {
            rectB = allInputObjects[j].getBoundingBox();
            matrix[i][j] = rectA.getMinimumVerticalDistanceTo(rectB);
            matrix[j][i] = matrix[i][j];
        }
    }
    if (N>1) {
        const tree = agnes(matrix, {method: 'single', isDistanceMatrix: true});
        let heights = [];
        tree.traverse(node => heights.push(node.height));
        heights = heights.filter(h => h>0);
        let cutoff = MathOps.median(heights)+1;
        let clusters = tree.cut(cutoff);

    }
    return null;
}
*/ 
