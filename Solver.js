"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};

var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};

var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.SecondarySolver = exports.PrimarySolver = void 0;

var Geometry_1 = require("./Geometry");
var fs = require("fs");
var lpsolve = require('lp_solve');
var Row = lpsolve.Row;

var PrimarySolver = /** @class */ (function () {
    function PrimarySolver(fields, texts, config) {
        var e_1, _a, e_2, _b;
        if (config === void 0) { config = null; }
        this.config = { optionsMax: true };
        this.fields = fields;
        this.texts = texts;
        
        if (config !== null)
            this.config = config;
        this.buildNormalizationBounds();
        this.progObject = new lpsolve.LinearProgram();
        var lp = this.progObject;
        this.s = [];
        this.a = [];
        var F = fields.length;
        var T = texts.length;

        this.map_fs = new Map();
        var map_fs = this.map_fs;
        this.map_ts = new Map(); // new
        var map_ts = this.map_ts;
        this.map_st = new Map();
        var map_st = this.map_st;
        var objective = new Row();
        var i = 0; // linear index

        for (var f = 0; f < F; f++) {
            map_fs.set(f, []);
            for (var t = 0; t < T; t++) {
                if (!map_ts.has(t))
                    map_ts.set(t, []);
                var s = lp.addColumn("s" + i, false, true);
                this.s.push(s);
                map_fs.get(f).push(i);
                map_ts.get(t).push(i);
                var c_1 = this.getDistance(fields[f], texts[t]);
                // console.log(`c${i} = ${c}`);
                objective = objective.Add(s, c_1); // ... + c_i S_i + ...
                map_st.set(i, t);
                i++;
            }
        }

        lp.setObjective(objective, true);
        var constraintsF = [];
        var constraintsT = [];
        var c = 0;

        for (var f = 0; f < F; f++) {
            constraintsF.push(new Row());
            var relevantStructuralColumnsIndexes = map_fs.get(f);
            try {
                for (var relevantStructuralColumnsIndexes_1 = (e_1 = void 0, __values(relevantStructuralColumnsIndexes)), relevantStructuralColumnsIndexes_1_1 = relevantStructuralColumnsIndexes_1.next(); !relevantStructuralColumnsIndexes_1_1.done; relevantStructuralColumnsIndexes_1_1 = relevantStructuralColumnsIndexes_1.next()) {
                    var s = relevantStructuralColumnsIndexes_1_1.value;
                    constraintsF[f] = constraintsF[f].Add(this.s[s], 1);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (relevantStructuralColumnsIndexes_1_1 && !relevantStructuralColumnsIndexes_1_1.done && (_a = relevantStructuralColumnsIndexes_1.return)) _a.call(relevantStructuralColumnsIndexes_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            lp.addConstraint(constraintsF[f], "EQ", 1, "a" + c++);
        }

        for (var t = 0; t < T; t++) {
            constraintsT.push(new Row());
            var relevantStructuralColumnsIndexes = map_ts.get(t);
            try {
                for (var relevantStructuralColumnsIndexes_2 = (e_2 = void 0, __values(relevantStructuralColumnsIndexes)), relevantStructuralColumnsIndexes_2_1 = relevantStructuralColumnsIndexes_2.next(); !relevantStructuralColumnsIndexes_2_1.done; relevantStructuralColumnsIndexes_2_1 = relevantStructuralColumnsIndexes_2.next()) {
                    var s = relevantStructuralColumnsIndexes_2_1.value;
                    constraintsT[t] = constraintsT[t].Add(this.s[s], 1);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (relevantStructuralColumnsIndexes_2_1 && !relevantStructuralColumnsIndexes_2_1.done && (_b = relevantStructuralColumnsIndexes_2.return)) _b.call(relevantStructuralColumnsIndexes_2);
                }
                finally { if (e_2) throw e_2.error; }
            }
            lp.addConstraint(constraintsT[t], "LE", 1, "a" + c++);
        }
        this.a = constraintsF.concat(constraintsT);
    }
    PrimarySolver.prototype.buildNormalizationBounds = function () {
        var e_3, _a, e_4, _b;
        var implicitTextFields = this.texts.filter(function (it) { return it.getAttribute("is-implicit-text") === true; });
        var explicitTextFields = this.texts.filter(function (it) { return it.getAttribute("is-implicit-text") !== true; });
        if (explicitTextFields.length === 0)
            explicitTextFields = implicitTextFields;
        var fieldBoxes = this.fields.map(function (field) { return field.getBoundingBox(); });
        var textBoxes = explicitTextFields.map(function (text) { return text.getBoundingBox(); });
        var min = +Infinity, max = -Infinity, distance = 0;
        try {
            for (var fieldBoxes_1 = __values(fieldBoxes), fieldBoxes_1_1 = fieldBoxes_1.next(); !fieldBoxes_1_1.done; fieldBoxes_1_1 = fieldBoxes_1.next()) {
                var fieldBox = fieldBoxes_1_1.value;
                try {
                    for (var textBoxes_1 = (e_4 = void 0, __values(textBoxes)), textBoxes_1_1 = textBoxes_1.next(); !textBoxes_1_1.done; textBoxes_1_1 = textBoxes_1.next()) {
                        var textBox = textBoxes_1_1.value;
                        distance = fieldBox.getMinimumDistanceTo(textBox);
                        if (distance < min)
                            min = distance;
                        if (distance > max)
                            max = distance;
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (textBoxes_1_1 && !textBoxes_1_1.done && (_b = textBoxes_1.return)) _b.call(textBoxes_1);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (fieldBoxes_1_1 && !fieldBoxes_1_1.done && (_a = fieldBoxes_1.return)) _a.call(fieldBoxes_1);
            }
            finally { if (e_3) throw e_3.error; }
        }
        var allBoxes = fieldBoxes.concat(textBoxes);
        var netBounds = Geometry_1.Rectangle.fromMergingOf(allBoxes);
        var diagonal = Math.sqrt(Math.pow(netBounds.getWidth(), 2) + Math.pow(netBounds.getHeight(), 2));
        this.geometricalDistanceBounds = [min, diagonal];
        var fontWeights = explicitTextFields.map(function (it) { return parseInt(it.computedStyle["font-weight"]); });
        var fontSizes = explicitTextFields.map(function (it) { return parseInt(it.computedStyle["font-size"]); });
        var weightedSizes = [];
        for (var i = 0, N = fontSizes.length; i < N; i++) {
            if (fontSizes[i] && fontWeights[i]) {
                weightedSizes.push(fontSizes[i] * fontWeights[i]);
            }
        }
        this.weightedFontSizeBounds = [Math.min.apply(Math, __spreadArray([], __read(weightedSizes), false)), Math.max.apply(Math, __spreadArray([], __read(weightedSizes), false))];
    };
    PrimarySolver.prototype.getDistance = function (field, text) {
        if (text.getAttribute("is-implicit-text") !== true) {
            var rectA = field.getBoundingBox();
            var rectB = text.getBoundingBox();
            var geometricScore = void 0, styleScore = void 0;
            if (this.config.optionsMax && field.isOptionField()) {
                geometricScore = (rectA.getCenterDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = 1 / ((parseInt(text.computedStyle["font-size"]) * parseInt(text.computedStyle["font-weight"])) / this.weightedFontSizeBounds[1]);
            }
            else {
                geometricScore = (rectA.getMinimumDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = (parseInt(text.computedStyle["font-size"]) * parseInt(text.computedStyle["font-weight"])) / this.weightedFontSizeBounds[1];
            }
            return geometricScore / styleScore;
        }
        else {
            if (field.getBoundingBox().contains(text.getBoundingBox()) === false)
                return +1000000; // simulate infinity for transfer to c++, because JS's Math.Infinity can not be passed to C++
            var rectA = field.getBoundingBox();
            var rectB = text.getBoundingBox().clone().expandBy(-this.geometricalDistanceBounds[0]);
            var geometricScore = void 0, styleScore = void 0;
            if (this.config.optionsMax && field.isOptionField()) {
                geometricScore = (rectA.getCenterDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = 1 / (this.weightedFontSizeBounds[0] / this.weightedFontSizeBounds[1]);
            }
            else {
                geometricScore = (rectA.getMinimumDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = this.weightedFontSizeBounds[0] / this.weightedFontSizeBounds[1];
            }
            return geometricScore / styleScore;
        }
    };

    PrimarySolver.prototype.solve = function () {
        // console.log("Program summary:")
        // console.log(this.progObject.dumpProgram());
        // console.log("\n");
        var e_5, _a;
        // let t0 = performance.now();
        var output = this.progObject.solve();
        // let tf = performance.now();
        // console.log(`total number of variables: ${this.s.length + this.a.length}  (${this.s.length} structural, ${this.a.length} auxiliary -- i.e. constraints)`)
        // console.log(`optimization result:  ${util.inspect(output)}`);
        // console.log(`optimization runtime: ${(tf-t0).toFixed(1)} milliseconds`);
        // console.log('objective =', this.progObject.getObjectiveValue());
        // console.log("");
        // for (let i=0; i<this.s.length; i++)  console.log(`s${i} = ${this.progObject.get(this.s[i])}`);
        // console.log("");
        // for (let i=0; i<this.a.length; i++)  console.log(`a${i} = ${this.progObject.calculate(this.a[i])}`);
        var result = new Map();
        var F = this.fields.length;
        if (output.description === "OPTIMAL") {
            for (var f = 0; f < F; f++) {
                var relevantStructuralColumnsIndexes = this.map_fs.get(f);
                try {
                    for (var relevantStructuralColumnsIndexes_3 = (e_5 = void 0, __values(relevantStructuralColumnsIndexes)), relevantStructuralColumnsIndexes_3_1 = relevantStructuralColumnsIndexes_3.next(); !relevantStructuralColumnsIndexes_3_1.done; relevantStructuralColumnsIndexes_3_1 = relevantStructuralColumnsIndexes_3.next()) {
                        var s = relevantStructuralColumnsIndexes_3_1.value;
                        if (this.progObject.get(this.s[s]) === 1) {
                            result.set(this.fields[f], this.texts[this.map_st.get(s)]);
                        }
                    }
                }
                catch (e_5_1) { e_5 = { error: e_5_1 }; }
                finally {
                    try {
                        if (relevantStructuralColumnsIndexes_3_1 && !relevantStructuralColumnsIndexes_3_1.done && (_a = relevantStructuralColumnsIndexes_3.return)) _a.call(relevantStructuralColumnsIndexes_3);
                    }
                    finally { if (e_5) throw e_5.error; }
                }
            }
            var constraints = [];
            for (var v in this.progObject.Constraints) {
                var c = this.progObject.Constraints[v];
                constraints.push(c.name + ': ' + c.row + ' ' + lpsolve.LinearProgram.ConstraintText[c.constraint] + ' ' + c.constant);
            }
            var finalSolution = [];
            for (var i = 0; i < this.s.length; i++)
                finalSolution.push({ decisionVariable: "s" + i, finalValue: this.progObject.get(this.s[i]) }); // (`s${i} = ${this.progObject.get(this.s[i])}`);
            for (var i = 0; i < this.a.length; i++)
                finalSolution.push({ auxVariable: "a" + i, finalValue: this.progObject.calculate(this.a[i]) }); // console.log(`a${i} = ${this.progObject.calculate(this.a[i])}`);
            fs.writeFileSync("primarySolution.json", JSON.stringify({
                problemType: this.progObject.ObjectiveFunc.minimize ? 'binary argmin' : 'binary argmax',
                objective: this.progObject.ObjectiveFunc.row.ToText(),
                constraints: constraints,
                solverStatus: output.description.toLowerCase(),
                finalSolution: finalSolution,
                output: this.progObject.getObjectiveValue(),
            }));
        }
        return result;
    };
    return PrimarySolver;
}());
exports.PrimarySolver = PrimarySolver;

var SecondarySolver = /** @class */ (function () {
    function SecondarySolver(fields, texts, config) {
        var e_6, _a, e_7, _b;
        if (config === void 0) { config = null; }
        this.config = { optionsMax: true };
        this.fields = fields;
        this.texts = texts;

        if (config !== null)
            this.config = config;
        this.buildNormalizationBounds();
        this.progObject = new lpsolve.LinearProgram();
        var lp = this.progObject;
        this.s = [];
        this.a = [];
        var F = fields.length;
        var T = texts.length;

        this.map_fs = new Map();
        var map_fs = this.map_fs;
        this.map_ts = new Map(); // new
        var map_ts = this.map_ts;
        this.map_st = new Map();
        var map_st = this.map_st;
        var objective = new Row();
        var i = 0; // linear index

        for (var f = 0; f < F; f++) {
            map_fs.set(f, []);
            for (var t = 0; t < T; t++) {
                if (!map_ts.has(t))
                    map_ts.set(t, []);
                var s = lp.addColumn("s" + i, false, true);
                this.s.push(s);
                map_fs.get(f).push(i);
                map_ts.get(t).push(i);
                var c_2 = this.getScore(fields[f], texts[t]);
                // console.log(`c${i} = ${c}`);
                objective = objective.Add(s, c_2); // ... + c_i S_i + ...
                map_st.set(i, t);
                i++;
            }
        }
        lp.setObjective(objective, false); // false == max
        var constraintsF = [];
        var constraintsT = [];
        var c = 0;

        for (var f = 0; f < F; f++) {
            constraintsF.push(new Row());
            var relevantStructuralColumnsIndexes = map_fs.get(f);
            try {
                for (var relevantStructuralColumnsIndexes_4 = (e_6 = void 0, __values(relevantStructuralColumnsIndexes)), relevantStructuralColumnsIndexes_4_1 = relevantStructuralColumnsIndexes_4.next(); !relevantStructuralColumnsIndexes_4_1.done; relevantStructuralColumnsIndexes_4_1 = relevantStructuralColumnsIndexes_4.next()) {
                    var s = relevantStructuralColumnsIndexes_4_1.value;
                    constraintsF[f] = constraintsF[f].Add(this.s[s], 1);
                }
            }
            catch (e_6_1) { e_6 = { error: e_6_1 }; }
            finally {
                try {
                    if (relevantStructuralColumnsIndexes_4_1 && !relevantStructuralColumnsIndexes_4_1.done && (_a = relevantStructuralColumnsIndexes_4.return)) _a.call(relevantStructuralColumnsIndexes_4);
                }
                finally { if (e_6) throw e_6.error; }
            }
            lp.addConstraint(constraintsF[f], "LE", 1, "a" + c++);
        }

        for (var t = 0; t < T; t++) {
            constraintsT.push(new Row());
            var relevantStructuralColumnsIndexes = map_ts.get(t);
            try {
                for (var relevantStructuralColumnsIndexes_5 = (e_7 = void 0, __values(relevantStructuralColumnsIndexes)), relevantStructuralColumnsIndexes_5_1 = relevantStructuralColumnsIndexes_5.next(); !relevantStructuralColumnsIndexes_5_1.done; relevantStructuralColumnsIndexes_5_1 = relevantStructuralColumnsIndexes_5.next()) {
                    var s = relevantStructuralColumnsIndexes_5_1.value;
                    constraintsT[t] = constraintsT[t].Add(this.s[s], 1);
                }
            }
            catch (e_7_1) { e_7 = { error: e_7_1 }; }
            finally {
                try {
                    if (relevantStructuralColumnsIndexes_5_1 && !relevantStructuralColumnsIndexes_5_1.done && (_b = relevantStructuralColumnsIndexes_5.return)) _b.call(relevantStructuralColumnsIndexes_5);
                }
                finally { if (e_7) throw e_7.error; }
            }
            lp.addConstraint(constraintsT[t], "LE", 1, "a" + c++);
        }
        this.a = constraintsF.concat(constraintsT);
    }
    SecondarySolver.prototype.buildNormalizationBounds = function () {
        var e_8, _a, e_9, _b;
        var implicitTextFields = this.texts.filter(function (it) { return it.getAttribute("is-implicit-text") === true; });
        var explicitTextFields = this.texts.filter(function (it) { return it.getAttribute("is-implicit-text") !== true; });
        if (explicitTextFields.length === 0)
            explicitTextFields = implicitTextFields;
        var fieldBoxes = this.fields.map(function (field) { return field.getBoundingBox(); });
        var textBoxes = explicitTextFields.map(function (text) { return text.getBoundingBox(); });
        var min = +Infinity, max = -Infinity, distance = 0;
        try {
            for (var fieldBoxes_2 = __values(fieldBoxes), fieldBoxes_2_1 = fieldBoxes_2.next(); !fieldBoxes_2_1.done; fieldBoxes_2_1 = fieldBoxes_2.next()) {
                var fieldBox = fieldBoxes_2_1.value;
                try {
                    for (var textBoxes_2 = (e_9 = void 0, __values(textBoxes)), textBoxes_2_1 = textBoxes_2.next(); !textBoxes_2_1.done; textBoxes_2_1 = textBoxes_2.next()) {
                        var textBox = textBoxes_2_1.value;
                        distance = fieldBox.getMinimumDistanceTo(textBox);
                        if (distance < min)
                            min = distance;
                        if (distance > max)
                            max = distance;
                    }
                }
                catch (e_9_1) { e_9 = { error: e_9_1 }; }
                finally {
                    try {
                        if (textBoxes_2_1 && !textBoxes_2_1.done && (_b = textBoxes_2.return)) _b.call(textBoxes_2);
                    }
                    finally { if (e_9) throw e_9.error; }
                }
            }
        }
        catch (e_8_1) { e_8 = { error: e_8_1 }; }
        finally {
            try {
                if (fieldBoxes_2_1 && !fieldBoxes_2_1.done && (_a = fieldBoxes_2.return)) _a.call(fieldBoxes_2);
            }
            finally { if (e_8) throw e_8.error; }
        }
        var allBoxes = fieldBoxes.concat(textBoxes);
        var netBounds = Geometry_1.Rectangle.fromMergingOf(allBoxes);
        var diagonal = Math.sqrt(Math.pow(netBounds.getWidth(), 2) + Math.pow(netBounds.getHeight(), 2));
        this.geometricalDistanceBounds = [min, diagonal];
        var fontWeights = explicitTextFields.map(function (it) { return parseInt(it.computedStyle["font-weight"]); });
        var fontSizes = explicitTextFields.map(function (it) { return parseInt(it.computedStyle["font-size"]); });
        var weightedSizes = [];
        for (var i = 0, N = fontSizes.length; i < N; i++) {
            if (fontSizes[i] && fontWeights[i]) {
                weightedSizes.push(fontSizes[i] * fontWeights[i]);
            }
        }
        this.weightedFontSizeBounds = [Math.min.apply(Math, __spreadArray([], __read(weightedSizes), false)), Math.max.apply(Math, __spreadArray([], __read(weightedSizes), false))];
    };
    SecondarySolver.prototype.getScore = function (field, text) {
        var fieldBox = field.getBoundingBox();
        var labelBox = text.getBoundingBox();
        if (labelBox.top >= fieldBox.bottom)
            return -1000000; // simulate infinity for transfer to c++, because JS's Math.Infinity can not be passed to C++
        if (text.getAttribute("is-implicit-text") !== true) {
            var rectA = field.getBoundingBox();
            var rectB = text.getBoundingBox();
            var geometricScore = void 0, styleScore = void 0;
            if (this.config.optionsMax && field.isOptionField()) {
                geometricScore = (rectA.getCenterDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = 1 / ((parseInt(text.computedStyle["font-size"]) * parseInt(text.computedStyle["font-weight"])) / this.weightedFontSizeBounds[1]);
            }
            else {
                geometricScore = (rectA.getMinimumDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = (parseInt(text.computedStyle["font-size"]) * parseInt(text.computedStyle["font-weight"])) / this.weightedFontSizeBounds[1];
            }
            return styleScore / geometricScore;
        }
        else {
            if (field.getBoundingBox().contains(text.getBoundingBox()) === false)
                return +1000000;
            var rectA = field.getBoundingBox();
            var rectB = text.getBoundingBox().clone().expandBy(-this.geometricalDistanceBounds[0]);
            var geometricScore = void 0, styleScore = void 0;
            if (this.config.optionsMax && field.isOptionField()) {
                geometricScore = (rectA.getCenterDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = 1 / (this.weightedFontSizeBounds[0] / this.weightedFontSizeBounds[1]);
            }
            else {
                geometricScore = (rectA.getMinimumDistanceTo(rectB) / this.geometricalDistanceBounds[1]);
                styleScore = this.weightedFontSizeBounds[0] / this.weightedFontSizeBounds[1];
            }
            return styleScore / geometricScore;
        }
    };

    SecondarySolver.prototype.solve = function () {
        // console.log("Program summary:")
        // console.log(this.progObject.dumpProgram());
        // console.log("\n");
        var e_10, _a;
        // let t0 = performance.now();
        var output = this.progObject.solve();
        // let tf = performance.now();
        // console.log(`total number of variables: ${this.s.length + this.a.length}  (${this.s.length} structural, ${this.a.length} auxiliary -- i.e. constraints)`)
        // console.log(`optimization result:  ${util.inspect(output)}`);
        // console.log(`optimization runtime: ${(tf-t0).toFixed(1)} milliseconds`);
        // console.log('objective =', this.progObject.getObjectiveValue());
        // console.log("");
        // for (let i=0; i<this.s.length; i++)  console.log(`s${i} = ${this.progObject.get(this.s[i])}`);
        // console.log("");
        // for (let i=0; i<this.a.length; i++)  console.log(`a${i} = ${this.progObject.calculate(this.a[i])}`);
        var result = new Map();
        var F = this.fields.length;
        if (output.description === "OPTIMAL") {
            for (var f = 0; f < F; f++) {
                var relevantStructuralColumnsIndexes = this.map_fs.get(f);
                try {
                    for (var relevantStructuralColumnsIndexes_6 = (e_10 = void 0, __values(relevantStructuralColumnsIndexes)), relevantStructuralColumnsIndexes_6_1 = relevantStructuralColumnsIndexes_6.next(); !relevantStructuralColumnsIndexes_6_1.done; relevantStructuralColumnsIndexes_6_1 = relevantStructuralColumnsIndexes_6.next()) {
                        var s = relevantStructuralColumnsIndexes_6_1.value;
                        if (this.progObject.get(this.s[s]) === 1) {
                            result.set(this.fields[f], this.texts[this.map_st.get(s)]);
                        }
                    }
                }
                catch (e_10_1) { e_10 = { error: e_10_1 }; }
                finally {
                    try {
                        if (relevantStructuralColumnsIndexes_6_1 && !relevantStructuralColumnsIndexes_6_1.done && (_a = relevantStructuralColumnsIndexes_6.return)) _a.call(relevantStructuralColumnsIndexes_6);
                    }
                    finally { if (e_10) throw e_10.error; }
                }
            }
            var constraints = [];
            for (var v in this.progObject.Constraints) {
                var c = this.progObject.Constraints[v];
                constraints.push(c.name + ': ' + c.row + ' ' + lpsolve.LinearProgram.ConstraintText[c.constraint] + ' ' + c.constant);
            }
            var finalSolution = [];
            for (var i = 0; i < this.s.length; i++)
                finalSolution.push({ decisionVariable: "s" + i, finalValue: this.progObject.get(this.s[i]) }); // (`s${i} = ${this.progObject.get(this.s[i])}`);
            for (var i = 0; i < this.a.length; i++)
                finalSolution.push({ auxVariable: "a" + i, finalValue: this.progObject.calculate(this.a[i]) }); // console.log(`a${i} = ${this.progObject.calculate(this.a[i])}`);
            fs.writeFileSync("secondarySolution.json", JSON.stringify({
                problemType: this.progObject.ObjectiveFunc.minimize ? 'binary argmin' : 'binary argmax',
                objective: this.progObject.ObjectiveFunc.row.ToText(),
                constraints: constraints,
                solverStatus: output.description.toLowerCase(),
                finalSolution: finalSolution,
                output: this.progObject.getObjectiveValue(),
            }));
        }
        return result;
    };
    return SecondarySolver;
}());
exports.SecondarySolver = SecondarySolver;
