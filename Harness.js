window.__HARNESS__ = {
    getROIs(root = document.body, parentXPath = "", nestedIFrames = false) {
        document.body.scrollIntoView()
        var ROIs = []
        findROIs(root, parentXPath)

        function findROIs(rootNode, xpathOfParent, parentRect = null) {
            if (parentRect == null) {
                parentRect = { top: 0, left: 0 }
            }
            var inIFrame = (xpathOfParent != "")
            var allElements = Array.from(rootNode.querySelectorAll("*"))
                .filter(element => __HARNESS__._isVisible(element, inIFrame, parentRect))

            // overlay images first
            allElements.forEach(element => {
                if (__HARNESS__._containsImage(element)) {
                    ROIs.push(__HARNESS__._getROIData(element, "image", xpathOfParent, parentRect))
                }
            })

            __HARNESS__.getTextROIs(rootNode, xpathOfParent, parentRect).forEach(ROI => ROIs.push(ROI))

            // then overlay input ROIs
            allElements.forEach(element => {
                if (__HARNESS__._containsInput(element)) {
                    ROIs.push(__HARNESS__._getROIData(element, "input", xpathOfParent, parentRect))
                }
            })

            if (nestedIFrames) {
                Array.from(rootNode.getElementsByTagName('iframe')).forEach(iframe => {
                    if (iframe.contentDocument != null) {
                        const relativeRect = iframe.getBoundingClientRect()
                        const absoluteRect = {
                            left: relativeRect.left + parentRect.left,
                            right: relativeRect.right + parentRect.left,
                            top: relativeRect.top + parentRect.top,
                            bottom: relativeRect.bottom + parentRect.top,
                            width: relativeRect.width,
                            height: relativeRect.height
                        }
                        var absoluteXPath = xpathOfParent + __HARNESS__._getXpathOfElement(iframe)
                        iframe.absoluteXPath = absoluteXPath
                        findROIs(iframe.contentDocument.body, absoluteXPath, absoluteRect)
                    }
                })
            }
        }

        function removeDuplicates(myArr, prop) {
            return myArr.filter((obj, pos, arr) => {
                return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
            });
        }

        //        return removeDuplicates(ROIs, 'xpath')
        try {
            window.__HARNESS___TURNOFF__()
        } catch (e) {}
        return ROIs
    },

    findHref(element) {
        while (true) {
            if (element.href !== undefined) {
                return element.href
            } else {
                if (element === document.body) {
                    return ""
                } else {
                    element = element.parentElement
                }
            }
        }
    },

    getTextROIs(rootNode, xpathOfParent = "", parentRect = null) {
        var xpath = '//*[not(self::script) and not(self::noscript) and not(self::style)]/text()'
        var textROIs = [],
            nextNode
        var textNodes
        try {
            textNodes = document.evaluate(xpath, rootNode, null, XPathResult.ANY_TYPE, null)
            while ((nextNode = textNodes.iterateNext()) != null) {
                if (nextNode.nodeValue.replace(/(&[^;]+;)/gi, "").replace(/\W+/gi, "").length > 0 &&
                    __HARNESS__._isVisible(nextNode.parentElement, parentRect)) {
                    textROIs.push(__HARNESS__._getROIData(nextNode, "text", xpathOfParent, parentRect))
                }
            }
        } catch (err) {}

        return textROIs
    },

    _getMBRRect(node, parentRect = null) {
        var finalRect
        if (parentRect == null) {
            parentRect = { left: 0, top: 0 }
        }
        if (node.nodeName == "#text") {
            var range = document.createRange()
            range.selectNode(node)
            var rects = Array.from(range.getClientRects())
            var rangeRect = range.getBoundingClientRect()
            rects.push(rangeRect)

            Array.from(node.parentElement.getClientRects()).forEach(rect => rects.push(rect))
            rects.push(node.parentElement.getBoundingClientRect())

            //            console.log("====== MBR Calculations ======")
            //            console.log("Text: " + node.textContent)
            //            rects.forEach(rect => console.log(rect))

            rects = rects.filter(rect => rect.width * rect.height >= 100)

            var rectDims = []
            for (var i = 0; i < rects.length; i++) {
                rectDims.push({ index: i, size: rects[i].width * rects[i].height })
            }
            rectDims.sort((a, b) => {
                    return (a.size - b.size)
                })
                //            console.log(" MBR is: ")
                //            console.log(rects[rectDims[0].index])
            range.detach()
                //        return rects[rectDims[0].index]

            finalRect = rangeRect
        } else {
            var rects = Array.from(node.getClientRects())
            rects.push(node.getBoundingClientRect())
            rects = rects.filter(rect => rect.width * rect.height >= 100)

            var rectDims = []
            for (var i = 0; i < rects.length; i++) {
                rectDims.push({ index: i, size: rects[i].width * rects[i].height })
            }
            rectDims.sort((a, b) => {
                return (a.size - b.size)
            })

            //            finalRect = rects[rectDims[0].index]
            finalRect = node.getBoundingClientRect()
        }

        finalRect.x = Math.round(finalRect.x + parentRect.left)
        finalRect.left = Math.round(finalRect.left + parentRect.left)
        finalRect.right = Math.round(finalRect.right + parentRect.left)
        finalRect.y = Math.round(finalRect.y + parentRect.top)
        finalRect.top = Math.round(finalRect.top + parentRect.top)
        finalRect.bottom = Math.round(finalRect.bottom + parentRect.top)
        finalRect.width = Math.round(finalRect.width)
        finalRect.height = Math.round(finalRect.height)
        return finalRect
    },

    _getROIData(ROINode, ROIType, xpathOfParent = "", parentRect = null) {
        var rect;
        var ROIElement;
        if (ROINode.nodeName == "#text") {
            ROIElement = ROINode.parentElement
        } else {
            ROIElement = ROINode
        }
        if (ROIType === "image") {
            if (__HARNESS__._isPseudoImage(ROIElement)) {
                rect = __HARNESS__._getPseudoImageRect(ROIElement);
            } else {
                rect = __HARNESS__.getBoundsWithOverflow(ROIElement);
            }
        } else if (ROIType === "text") {
            var range = document.createRange();
            range.selectNode(ROINode);
            rect = range.getBoundingClientRect();
        } else {
            rect = __HARNESS__._getMBRRect(ROINode, parentRect)
        }

        var ROIData = {
            x1: Math.round(rect.left),
            x: Math.round(rect.left),
            left: Math.round(rect.left),

            x2: Math.round(rect.right),
            right: Math.round(rect.right),

            y1: Math.round(rect.top),
            y: Math.round(rect.top),
            top: Math.round(rect.top),

            y2: Math.round(rect.bottom),
            bottom: Math.round(rect.bottom),

            width: Math.round(rect.width),
            height: Math.round(rect.height),
            xpath: xpathOfParent + __HARNESS__._getXpathOfElement(ROIElement),
            element: ROIElement // <- REMOVE THIS FOR SELENIUM INTEGRATION
        }
        if (ROIType === "text") {
            ROIData.type = "text"
            ROIData.content = ROINode.textContent
        } else if (ROIType === "image") {
            ROIData.type = "image"
        } else if (ROIType === "input") {
            ROIData.type = "input - type=" + ROIElement.type + " - " + [ROIElement.value, ROIElement.placeholder, ROIElement.title].join(' ')
        } else {
            throw new Error("unrecognized ROI type '" + ROIType + "'.")
        }

        ROIData.attributes = Array.from(ROIElement.attributes).map(it => ({ "attribute": it.nodeName, "value": (it.nodeValue + "").toLowerCase() }));
        ROIData.attributes.push({ "attribute": "data-cortex-implicit-text", "value": __HARNESS__.getImplicitText(ROIElement) });
        if (ROIType === "text") {
            ROIData.attributes.push({ "attribute": "textContent", "value": ROIElement.textContent });
            ROIData.attributes.push({ "attribute": "innerText", "value": ROIElement.innerText });
        }

        ROIData.href = __HARNESS__.findHref(ROIElement);
        var style = getComputedStyle(ROIElement);
        ROIData.computedStyle = this.computedStyleToPlainObject(style);
        ROIData.fontSize = parseInt(style.fontSize) + parseInt(style.fontWeight)
        ROIData.foregroundColor = __HARNESS__._getRGBAVector(style.color)
        ROIData.backgroundColor = __HARNESS__._getRGBAVector(style.backgroundColor)
        ROIData.isClickable = style['cursor'] === 'pointer' || (style['cursor'] === 'default' && ['BUTTON', 'INPUT', 'A'].includes(ROIElement.tagName))
        if (window.__HARNESS___CL__ !== undefined) { ROIData.isClickable = ROIData.isClickable || window.__HARNESS___CL__.includes(ROIElement) }
        //        ROIData.hash = this._hashCode(ROIData)
        //        ROIData.element = ROIElement // Remove field 'element' before connecting to Selenium

        if (window.OptimalSelect !== undefined) {
            ROIData.cssSelector = OptimalSelect.select(ROIElement);
        } else {
            ROIData.cssSelector = "Error: please load OptimalSelect library first"
        }

        return ROIData
    },

    computedStyleToPlainObject(style) {
        let object = {};
        let prop, val;
        for (let i = 0, N = style.length; i < N; i++) {
            prop = style.item(i);
            val = style.getPropertyValue(prop);
            val = val === "" ? undefined : val;
            object[prop] = val;
        }
        return object;
    },

    showROIImage() {
        const ROIs = this.getROIs()
            //        const ROIs = this.getAllROIs()
        this._addBackgroundOverlay()
        this._addROIsOverlay(ROIs)
    },

    removeROIImage() {
        this._removeOverlays()
    },

    _addROIsOverlay(ROIs) {
        const backgroundOverlay = document.querySelector("#__cortex_background_overlay__")
        ROIs.forEach(ROI => {
            var content = ROI.content ? ROI.content : ""
            var ROIElement = document.createElement("div")
            var backgroundColor;
            if (ROI.type.includes("text")) backgroundColor = "rgba(0, 255, 0, 0.3)";
            if (ROI.type.includes("image")) backgroundColor = "rgba(0, 0, 255, 0.3)";
            if (ROI.type.includes("input")) backgroundColor = "rgba(255, 0, 0, 0.3)";

            ROIElement.style.cssText = "\
            position: absolute; top: " + ROI.y1 + "px; left: " + ROI.x1 + "px; \
            width: " + ROI.width + "px; height: " + ROI.height + "px; \
            background-color: " + backgroundColor + "; border: 1px solid black;"
            ROIElement.setAttribute('title', ROI.type + "\n" + ROI.xpath)
                // Remove the following event listener before connecting to Selenium
            ROIElement.addEventListener("click", () => {
                console.log(ROI.element)
            })
            backgroundOverlay.appendChild(ROIElement)
        })
    },

    _addBackgroundOverlay() {
        if (!document.querySelector("#__cortex_background_overlay__")) {
            const height = document.documentElement.scrollHeight
            const width = document.documentElement.scrollWidth
            const overlay = document.createElement("div")
            overlay.id = "__cortex_background_overlay__"
            overlay.style.cssText = "\
            z-index: 1000000; \
            background-color: rgba(0,0,0, 0.4); \
            position: absolute; top: 0px; left: 0px; \
            width: " + width + "px; height: " + height + "px;"
            document.body.appendChild(overlay)
        }
    },

    _removeOverlays() {
        document.body.removeChild(
            document.querySelector("#__cortex_background_overlay__")
        )
    },

    _confirmExistence(element) {
        const rect = __HARNESS__.getBoundsWithOverflow(element)
        const cx = rect.x + (rect.width / 2),
            cy = rect.y + (rect.height / 2)

        return __HARNESS__._retrieveByLocation(cx, cy).includes(__HARNESS__._getXpathOfElement(element))
    },

    _retrieveByLocation(x, y) {
        const elements = document.elementsFromPoint(x, y)
        var result = []
        for (var i = 0; i < elements.length; i++) {
            if (elements[i] != null && !["BODY", "HTML"].includes(elements[i].tagName)) {
                result.push(__HARNESS__._getXpathOfElement(elements[i]))
            }
        }
        return result
    },

    _isVisible(element, inIFrame = false, parentRect) {
        const style = getComputedStyle(element);
        const rect = element.getBoundingClientRect()

        if (element.nodeName === "INPUT" && "checkbox|radio".includes(element.type) && (rect.width * rect.height > 0) && (rect.left * rect.top > 0)) return true;

        if (element.nodeName === "SCRIPT" || element.nodeName === "NOSCRIPT") return false;
        if (style.display === 'none') return false;
        if (style.visibility !== 'visible') return false;
        if (style['clip'] !== 'auto' || style['clipPath'] !== 'none') return false;
        if (parseInt(style.width) <= 1 || parseInt(style.height) <= 1) return false;
        //        if (style.opacity < 0.1) return false;
        if (element.offsetWidth + element.offsetHeight + rect.height + rect.width === 0) return false;
        if (parentRect == null || parentRect == undefined) {
            parentRect = { left: 0, top: 0 }
        }
        const elementCenter = {
            x: parentRect.left + rect.left + (rect.width / 2),
            y: parentRect.top + rect.top + (rect.height / 2)
        };
        // Uncomment the following 'elementsFromPoint' line before using with Selenium
        //        if (document.elementsFromPoint(elementCenter.x, elementCenter.y).indexOf(element)<0) return false;
        if (!inIFrame) {
            if (elementCenter.x < 0) return false;
            if (elementCenter.x > document.documentElement.scrollWidth) return false;
            if (elementCenter.y < 0) return false;
            if (elementCenter.y > document.documentElement.scrollHeight) return false;
        }
        return true;
    },

    isTextEntry(element) {
        let tag = element.tagName.toLowerCase();

        if (tag === "select" || tag === "textarea" || (tag === "input" && "date|email|number|number|password|search|tel|text|url".includes(element.type.toLowerCase()))) {
            return true;
        } else {
            return false;
        }
    },

    getImplicitText(element) {
        if (__HARNESS__.isTextEntry(element)) {
            let tag = element.tagName.toLowerCase();

            switch (tag) {
                case "select":
                    let options = Array.from(element.querySelectorAll("option"));
                    let displayed = options.filter(option => option.selected)[0];
                    return displayed ? displayed.text : "";
                case "textarea":
                    return element.textContent ? element.textContent : element.placeholder;
                case "input":
                    return element.value ? element.value : (element.placeholder ? element.placeholder : element.innerText);
            }
        } else {
            return undefined;
        }
    },

    _getXpathOfElement(element) {
        var paths = [];
        for (; element && element.nodeType == 1; element = element.parentNode) {
            var index = 0;
            for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling) {
                if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
                    continue;

                if (sibling.nodeName == element.nodeName)
                    ++index;
            }
            var tagName = element.nodeName.toLowerCase();
            var pathIndex = "[" + (index + 1) + "]";
            paths.splice(0, 0, tagName + pathIndex);
        }
        return paths.length ? "/" + paths.join("/") : null;
    },

    _getElementFromXpath(xpath) {
        try {
            return document.evaluate(xpath, document, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null).singleNodeValue
        } catch (err) {
            return null
        }
    },

    _isNonEmptyString(str) {
        const newStr = str.toString();
        return newStr.replace(/\s/g, '').replace(/&nbsp;/g, '') != '';
    },

    _containsText(element) {
        var childNodes = element.childNodes;
        for (var n = 0; n < childNodes.length; n++) {
            if (childNodes[n].nodeType == 3 &&
                childNodes[n].nodeName === "#text" &&
                this._isNonEmptyString(childNodes[n].nodeValue)) {
                return true;
            }
        }
        return false;
    },

    _containsInput(element) {
        if (element.nodeName === "INPUT" || element.nodeName === "SELECT" ||
            element.nodeName === "TEXTAREA" || element.nodeName === "BUTTON") {
            return true
        } else {
            return false
        }
    },

    _mergeRects(rects) {
        let minX = Infinity,
            minY = Infinity,
            maxX = -Infinity,
            maxY = -Infinity;

        for (const rect of rects) {
            if (rect.left < minX) minX = rect.left;
            if (rect.top < minY) minY = rect.top;
            if (rect.right > maxX) maxX = rect.right;
            if (rect.bottom > maxY) maxY = rect.bottom;
        }

        if (rects.length === 0) {
            minX = minY = maxX = maxY = 0;
        }

        return {
            x: minX,
            left: minX,
            y: minY,
            top: minY,
            bottom: maxY,
            right: maxX,
            width: maxX - minX,
            height: maxY - minY
        };
    },

    _getPseudoImageRect(element) {
        let childNodes = element.childNodes;
        let childRects = [];
        var range = document.createRange();
        let isBefore = false,
            isAfter = false;
        for (let i = 0; i < childNodes.length; i++) {
            range.selectNodeContents(childNodes[i]);
            childRects.push(range.getBoundingClientRect());
        }

        if (getComputedStyle(element, "::before").backgroundImage.substring(0, 4) === 'url(') {
            isBefore = true;
        }

        if (getComputedStyle(element, "::after").backgroundImage.substring(0, 4) === 'url(') {
            isAfter = true;
        }

        // range.selectNodeContents(element);
        // let elementRect = range.getBoundingClientRect();
        let elementRect = element.getBoundingClientRect();

        if (isAfter && isBefore) {
            return elementRect;
        }

        let mergedChildRects = __HARNESS__._mergeRects(childRects);
        let pseudoImageRect;

        if (isBefore) {
            pseudoImageRect = elementRect.toJSON();
            pseudoImageRect.right = mergedChildRects.left;
            pseudoImageRect.width = pseudoImageRect.right - pseudoImageRect.left;
            return pseudoImageRect;
        }

        if (isAfter) {
            pseudoImageRect = elementRect.toJSON();
            pseudoImageRect.left = mergedChildRects.right;
            pseudoImageRect.x = pseudoImageRect.left;
            pseudoImageRect.width = pseudoImageRect.right - pseudoImageRect.left;
            return pseudoImageRect;
        }

        return elementRect;
    },

    _isPseudoImage(element) {
        return (getComputedStyle(element, "::before").backgroundImage.substring(0, 4) === 'url(' ||
            getComputedStyle(element, "::after").backgroundImage.substring(0, 4) === 'url(');
    },

    _containsImage(element) {
        const rect = element.getBoundingClientRect()
        if (rect.width * rect.height >= 100) {
            if (element.nodeName == "IMG" && element.src != "") {
                return true
            } else if (element.nodeName == "svg") {
                return true
            } else if (element.nodeName == "VIDEO") {
                return true
            } else if (getComputedStyle(element).backgroundImage.substring(0, 4) === 'url(') {
                return true
            } else if (__HARNESS__._isPseudoImage(element)) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    },

    _getRGBAVector(rgba) {
        rgba = rgba.replace(/[^\d,.]/g, '').split(',').map(val => parseFloat(val))
        if (rgba.length == 4) {
            rgba[3] = Math.round(rgba[3] * 255)
        } else if (rgba.length == 3) {
            rgba.push(255)
        } else {
            throw new Error("Unknown CSS RGBA color string format.")
        }
        return rgba
    },

    _hashCode(object) {
        const jsonStr = JSON.stringify(object)
        var hash = 0;
        if (jsonStr.length === 0) {
            return hash;
        }
        for (var i = 0; i < jsonStr.length; i++) {
            var char = jsonStr.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash;
        }
        return hash;
    },

    g_madeHiddenROIs: new Map(),

    transparentOn(targetROIs) {
        var ROIs, element
        if (__HARNESS__.g_madeHiddenROIs.size != 0) {
            // do nothing; already hidden
            // ROIs = __HARNESS__.g_madeHiddenROIs
        } else {
            ROIs = targetROIs || __HARNESS__.getROIs()
            ROIs.forEach(ROI => {
                // element = __HARNESS__._getElementFromXpath(ROI.xpath)
                element = ROI.element
                if (element != null) {
                    if (ROI.type == "text") {
                        __HARNESS__.g_madeHiddenROIs.set(element, { type: "text", color: element.style.color })
                        __HARNESS__.makeTextTransparent(element)
                    } else {
                        __HARNESS__.g_madeHiddenROIs.set(element, { type: "non-text", opacity: element.style.opacity })
                        element.style.opacity = 0.001
                    }
                } else {
                    console.log("null element")
                }
            })
        }
    },

    transparentOff() {
        var ROIs, element, storedVal
        if (__HARNESS__.g_madeHiddenROIs.size != 0) {
            ROIs = __HARNESS__.g_madeHiddenROIs
            ROIs.forEach((value, key, map) => {
                element = key
                storedVal = value
                if (storedVal.type == "text") {
                    element.style.color = storedVal.color
                } else {
                    element.style.opacity = storedVal.opacity
                }
            })
            __HARNESS__.g_madeHiddenROIs.clear()
        } else {
            // do nothing; nothing is hidden
        }
    },

    getBoundsWithOverflow(element) {
        var bounds = element.getBoundingClientRect()
        var parentRect
        while (element.parentElement != null && element.parentElement.tagName != "BODY") {
            if (__HARNESS__._isVisible(element.parentElement)) {
                if (getComputedStyle(element.parentElement)['overflow'] != "visible") {
                    if (__HARNESS__.isProtruding(element.parentElement, element)) {
                        //                         console.log("bp1")
                        parentRect = element.parentElement.getBoundingClientRect()
                            //                         console.log("bp2")
                        bounds = __HARNESS__.trimRectAtoB(bounds, parentRect)
                            //                         console.log("bp3")

                    }
                }
            }
            element = element.parentElement
        }
        return bounds
    },

    isProtruding(parent, child) {
        var parentRect = parent.getBoundingClientRect()
        var childRect = child.getBoundingClientRect()
        var parentCenter = {
            x: parentRect.left + (parentRect.width / 2),
            y: parentRect.top + (parentRect.height / 2)
        }
        var childCenter = {
            x: childRect.left + (childRect.width / 2),
            y: childRect.top + (childRect.height / 2)
        }

        if (__HARNESS__.isPointInside(childCenter, parentRect)) {
            if (childRect.left < parentRect.left) return true
            if (childRect.right > parentRect.right) return true
            if (childRect.top < parentRect.top) return true
            if (childRect.bottom > parentRect.bottom) return true
            return false
        } else {
            return true
        }
    },

    trimRectAtoB(A, B) {
        var trimmed = {
            x: A.x,
            y: A.y,
            width: A.width,
            height: A.height,
            top: A.top,
            right: A.right,
            bottom: A.bottom,
            left: A.left
        }

        if (A.left < B.left) trimmed.left = B.left
        if (A.x < B.x) trimmed.x = B.x

        if (A.top < B.top) trimmed.top = B.top
        if (A.y < B.y) trimmed.y = B.y

        if (A.right > B.right) trimmed.right = B.right
        if (A.bottom > B.bottom) trimmed.bottom = B.bottom
        trimmed.width = trimmed.right - trimmed.left
        trimmed.height = trimmed.bottom - trimmed.top
        return trimmed
    },

    isPointInside(point, rect) {
        if (point.x <= rect.right && point.x >= rect.left &&
            point.y <= rect.bottom && point.y >= rect.top) {
            return true
        } else {
            return false
        }
    },

    getEffectiveBGColor(element) {
        var colorArray = __HARNESS__._getBGColorArray(element)
        var effective = colorArray.reduceRight((bg, fg) => {
            var a = fg[3] / 255
            return [a * fg[0] + (1 - a) * bg[0],
                a * fg[1] + (1 - a) * bg[1],
                a * fg[2] + (1 - a) * bg[2],
                255
            ]
        }, [255, 255, 255, 255])
        effective.pop()
        return effective.map(channel => Math.round(channel))
    },

    makeTextTransparent(element) {
        var color = __HARNESS__.getEffectiveBGColor(element)
        element.style.color = "rgb(" + color[0] + ", " + color[1] + "," + color[2] + ")"
    },

    _getBGColorArray(element) {
        var bgColorArray = []
        while (element != null) {
            style = getComputedStyle(element)
            bgColorArray.push(__HARNESS__._getRGBAVector(style['backgroundColor']))
            element = element.parentElement
        }
        return bgColorArray
    },


    findGroupRootsAt(rootXPath) {
        var root = __HARNESS__._getElementFromXpath(rootXPath)
        var groupRoots = []
        if (root != null) {
            for (element of root.children) {
                if (element != null && ["SCRIPT", "NOSCRIPT", "IFRAME", "STYLE", "BUTTON", "INPUT", "TEXTAREA", "SPAN", "svg"].includes(element.tagName) == false) {
                    //                    if (__HARNESS__._confirmExistence(element)) {
                    var box = element.getBoundingClientRect()
                    groupRoots.push({
                            rootXPath: __HARNESS__._getXpathOfElement(element),
                            left: box.left,
                            right: box.right,
                            top: box.top,
                            bottom: box.bottom,
                            width: box.width,
                            height: box.height
                        })
                        //                    }
                }
            }
        }
        return groupRoots
    },

    extractAllGroups() {
        /*
        Array.from(document.querySelectorAll('*')).filter(element => !["SCRIPT", "HTML",
        "BODY", "HEAD", "LINK", "TITLE", "META", "NOSCRIPT", "IFRAME", "style", "STYLE",
        "BUTTON", "INPUT", "TEXTAREA", "SPAN", "svg", "IMG", "CANVAS"]
        .includes(element.tagName)).forEach(el=>console.log(el.textContent))

         */
        return Array.from(document.querySelectorAll('*')).filter(element => !["SCRIPT",
            "HTML", "BODY", "HEAD", "LINK", "TITLE", "META", "NOSCRIPT", "IFRAME", "style", "STYLE",
            "BUTTON", "INPUT", "TEXTAREA", "SPAN", "svg", "IMG", "CANVAS"
        ].includes(element.tagName)).map((element) => {
            let xpath, css;
            try {
                xpath = __HARNESS__._getXpathOfElement(element);
            } catch (err) {
                // throw new Error("unable to get xpath.") 
                xpath = null;
            }

            try {
                css = OptimalSelect.select(element);
            } catch (err) {
                // throw new Error("unable to get css selector.") 
                css = null;
            }

            var box = element.getBoundingClientRect();
            return {
                textContent: element.textContent,
                rootXPath: xpath,
                cssSelector: css,
                x: box.x,
                y: box.y,
                left: box.left,
                right: box.right,
                top: box.top,
                bottom: box.bottom,
                width: box.width,
                height: box.height
            }
        })
    },

    findHeadings() {
        const _isolate = (e) => ({
            level: e.getAttribute('aria-level') ? e.getAttribute('aria-level') : e.nodeName.substr(1)
        })
        let headings = Array.from(document.body.querySelectorAll("h1,h2,h3,h4,h5,h6,[role='heading']"));
        let map = { all: headings.map(h => __HARNESS__._getXpathOfElement(h)) };
        [1, 2, 3, 4, 5, 6].forEach(l => map[`${l}`] = headings.filter(element => element.nodeName === `H${l}` ||
                !element.nodeName.startsWith("H") && element.getAttribute('aria-level') === `${l}`)
            .map(h => __HARNESS__._getXpathOfElement(h)));
        return map
    },

    _getAllXPathsBetween(A, B) {
        var elA = __HARNESS__._getElementFromXpath(A)

        var A_children = Array.from(elA.querySelectorAll("*"))
        var finalList = []

        A_children.forEach(child => {
            var xpath = __HARNESS__._getXpathOfElement(child)
            if (!xpath.includes(B)) {
                finalList.push(xpath)
            }
        })

        return finalList
    },

    hasHeadingBetween(xpathA, xpathB, headingLevel) {
        var elementsInBetween = __HARNESS__._getAllXPathsBetween(xpathA, xpathB)

        elementsInBetween.forEach(xpath => {
            var element = __HARNESS__._getElementFromXpath(xpath)
            if (element != null && element.tagName == ("H" + headingLevel)) {
                return true
            }
        })

        return false
    },


    hasRoleBetween(xpathA, xpathB, role) {
        var elementsInBetween = __HARNESS__._getAllXPathsBetween(xpathA, xpathB)

        elementsInBetween.forEach(xpath => {
            var element = __HARNESS__._getElementFromXpath(xpath)
            if (element.getAttribute('role') == role) {
                return true
            }
        })

        return false
    },


    findPseudoElements() {
        var allElements = Array.from(document.body.querySelectorAll("*"))
        var beforePseudos = allElements.filter(element => {
            return getComputedStyle(element, "::before").backgroundImage.includes("url")
        })
        var afterPseudos = allElements.filter(element => {
            return getComputedStyle(element, "::after").backgroundImage.includes("url")
        })
        return { beforePseudos, afterPseudos }
    },


    hidePseudoElements(pseudoElements) {
        pseudoElements = pseudoElements.beforePseudos.concat(pseudoElements.afterPseudos)
        var className = "hidePseudo" + Math.random().toString(36).substring(2, 15)
        var hide = document.createElement("style")
        hide.innerHTML = `
            .${className}::after { background-image: none !important; }
            .${className}::before { background-image: none !important; }
        `
        document.head.appendChild(hide)
        pseudoElements.forEach(element => element.className += " " + className)
    }

}