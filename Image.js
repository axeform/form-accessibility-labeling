"use strict";
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.Image = void 0;
var opencv_js_1 = require("../lib/opencv.js");
var jimp_1 = __importDefault(require("jimp"));
var Util_1 = require("./Util");
var Geometry_1 = require("./Geometry");
var canvas_1 = require("canvas");

var Image = /** @class */ (function () {
    function Image() {
    }

    Image.fromScratch = function (width, height, numberOfChannels) {
        if (numberOfChannels === void 0) { numberOfChannels = 1; }
        var numChEnum;
        switch (numberOfChannels) {
            case 1:
                numChEnum = opencv_js_1.cv.CV_8UC1;
                break;
            case 3:
                numChEnum = opencv_js_1.cv.CV_8UC3;
                break;
            case 4:
                numChEnum = opencv_js_1.cv.CV_8UC4;
                break;
            default:
                throw new Error("number of channels must be 1, 3, or 4.");
        }
        var newInstance = new Image();
        newInstance.mat = opencv_js_1.cv.Mat.zeros(height, width, numChEnum);
        return newInstance;
    };

    Image.fromBase64 = function (base64) {
        var imgBuffer = Buffer.from(base64, "base64");
        var image = (0, Util_1.executeSync)(jimp_1.default.read(imgBuffer));
        var newInstance = new Image();
        newInstance.mat = opencv_js_1.cv.matFromImageData(image.bitmap);
        return newInstance;
    };

    Image.prototype.clone = function () {
        var newInstance = new Image();
        newInstance.mat = this.mat.clone();
        return newInstance;
    };

    Image.prototype.cloneRegion = function (region) {
        var width = region.getWidth();
        var height = region.getHeight();
        var newMat = opencv_js_1.cv.Mat.zeros(height, width, this.mat.type());
        var rect = new opencv_js_1.cv.Rect(region.left, region.top, width, height);
        var roi = this.mat.roi(rect);
        roi.copyTo(newMat);
        roi.delete();
        var newInstance = new Image();
        newInstance.mat = newMat;
        return newInstance;
    };

    Image.prototype.getBoundingBoxes = function () {
        if (this.mat.type() !== opencv_js_1.cv.CV_8UC1) {
            throw new Error("image must be 8-bits single channel.");
        }
        var contours = new opencv_js_1.cv.MatVector();
        var hierarchy = new opencv_js_1.cv.Mat();
        opencv_js_1.cv.findContours(this.mat, contours, hierarchy, opencv_js_1.cv.RETR_LIST, opencv_js_1.cv.CHAIN_APPROX_SIMPLE);
        var rect, boxes = [];
        for (var i = 0, n = contours.size(); i < n; i++) {
            rect = opencv_js_1.cv.boundingRect(contours.get(i));
            // boxes.push([rect.x, rect.y, rect.width, rect.height]);
            boxes.push(new Geometry_1.Rectangle(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height));
        }
        hierarchy.delete();
        return boxes;
    };

    Image.prototype.absoluteDifference = function (other) {
        if (this.getHeight() !== other.getHeight() ||
            this.getWidth() !== other.getWidth() ||
            this.getNumberOfChannels() !== other.getNumberOfChannels()) {
            throw new Error("images must have the same channels.");
        }
        var dst = new opencv_js_1.cv.Mat();
        opencv_js_1.cv.absdiff(this.mat, other.mat, dst);
        if (dst.channels() === 4) {
            var channels = new opencv_js_1.cv.MatVector();
            opencv_js_1.cv.split(dst, channels);
            channels.set(3, new opencv_js_1.cv.Mat(this.mat.rows, this.mat.cols, opencv_js_1.cv.CV_8UC1, new opencv_js_1.cv.Scalar(255)));
            opencv_js_1.cv.merge(channels, dst);
            channels.delete();
        }
        this.mat.delete();
        this.mat = dst;
        return this;
    };

    Image.prototype.subtract = function (other) {
        if (this.getHeight() !== other.getHeight() ||
            this.getWidth() !== other.getWidth() ||
            this.getNumberOfChannels() !== other.getNumberOfChannels()) {
            throw new Error("images must have the same channels.");
        }
        var dst = new opencv_js_1.cv.Mat();
        var mask = new opencv_js_1.cv.Mat();
        opencv_js_1.cv.subtract(this.mat, other.mat, dst, mask, -1);
        mask.delete();
        if (dst.channels() === 4) {
            var channels = new opencv_js_1.cv.MatVector();
            opencv_js_1.cv.split(dst, channels);
            channels.set(3, new opencv_js_1.cv.Mat(this.mat.rows, this.mat.cols, opencv_js_1.cv.CV_8UC1, new opencv_js_1.cv.Scalar(255)));
            opencv_js_1.cv.merge(channels, dst);
            channels.delete();
        }
        this.mat.delete();
        this.mat = dst;
        return this;
    };

    Image.prototype.toGray = function () {
        if (this.getNumberOfChannels() === 4) {
            opencv_js_1.cv.cvtColor(this.mat, this.mat, opencv_js_1.cv.COLOR_RGBA2GRAY, 0);
        }
        if (this.getNumberOfChannels() === 3) {
            opencv_js_1.cv.cvtColor(this.mat, this.mat, opencv_js_1.cv.COLOR_RGB2GRAY, 0);
        }
        return this;
    };

    Image.prototype.threshold = function (value) {
        opencv_js_1.cv.threshold(this.mat, this.mat, value, 255, opencv_js_1.cv.THRESH_BINARY);
        return this;
    };

    Image.prototype.dilate = function (dilationSize) {
        var dst = new opencv_js_1.cv.Mat();
        // let kernel = cv.Mat.ones(dilationSize, dilationSize, cv.CV_8U);
        var kernel = opencv_js_1.cv.getStructuringElement(opencv_js_1.cv.MORPH_ELLIPSE, new opencv_js_1.cv.Size(dilationSize, dilationSize)); // other possible values: cv.MORPH_CROSS, cv.MORPH_RECT, cv.MORPH_ELLIPSE
        var anchor = new opencv_js_1.cv.Point(-1, -1);
        opencv_js_1.cv.dilate(this.mat, dst, kernel, anchor, 1, opencv_js_1.cv.BORDER_CONSTANT, opencv_js_1.cv.morphologyDefaultBorderValue());
        this.mat.delete();
        kernel.delete();
        this.mat = dst;
        return this;
    };

    Image.prototype.erode = function (erosionSize) {
        var dst = new opencv_js_1.cv.Mat();
        var kernel = opencv_js_1.cv.getStructuringElement(opencv_js_1.cv.MORPH_ELLIPSE, new opencv_js_1.cv.Size(erosionSize, erosionSize)); // other possible values: cv.MORPH_CROSS, cv.MORPH_RECT, cv.MORPH_ELLIPSE
        var anchor = new opencv_js_1.cv.Point(-1, -1);
        opencv_js_1.cv.erode(this.mat, dst, kernel, anchor, 1, opencv_js_1.cv.BORDER_CONSTANT, opencv_js_1.cv.morphologyDefaultBorderValue());
        this.mat.delete();
        kernel.delete();
        this.mat = dst;
        return this;
    };

    Image.prototype.resizeTo = function (width, height) {
        var dst = new opencv_js_1.cv.Mat();
        opencv_js_1.cv.resize(this.mat, dst, new opencv_js_1.cv.Size(width, height), 0, 0, opencv_js_1.cv.INTER_LINEAR);
        this.mat.delete();
        this.mat = dst;
        return this;
    };

    Image.prototype.resizeBy = function (factorWidth, factorHeight) {
        var width = Math.floor(factorWidth * this.getWidth());
        var height = Math.floor(factorHeight * this.getHeight());
        this.resizeTo(width, height);
        return this;
    };

    Image.prototype.resizeToMatch = function (other) {
        this.resizeTo(other.getWidth(), other.getHeight());
        return this;
    };

    Image.prototype.appendBelow = function (otherImage) {
        var sizeA = this.getSize();
        var sizeB = otherImage.getSize();
        if (sizeA[2] !== sizeB[2]) {
            throw new Error("images must have the same number of channels.");
        }
        var width = Math.max(sizeA[0], sizeB[0]);
        var height = sizeA[1] + sizeB[1];
        var newMat = opencv_js_1.cv.Mat.zeros(height, width, this.mat.type());
        var roiA = newMat.roi(new opencv_js_1.cv.Rect(0, 0, sizeA[0], sizeA[1]));
        var roiB = newMat.roi(new opencv_js_1.cv.Rect(0, sizeA[1], sizeB[0], sizeB[1]));
        this.mat.copyTo(roiA);
        otherImage.mat.copyTo(roiB);
        this.mat.delete();
        this.mat = newMat;
        roiA.delete();
        roiB.delete();
        return this;
    };

    Image.prototype.appendRight = function (otherImage) {
        var sizeA = this.getSize();
        var sizeB = otherImage.getSize();
        if (sizeA[2] !== sizeB[2]) {
            throw new Error("images must have the same number of channels.");
        }
        var width = sizeA[0] + sizeB[0];
        var height = Math.max(sizeA[1], sizeB[1]);
        var newMat = opencv_js_1.cv.Mat.zeros(height, width, this.mat.type());
        var roiA = newMat.roi(new opencv_js_1.cv.Rect(0, 0, sizeA[0], sizeA[1]));
        var roiB = newMat.roi(new opencv_js_1.cv.Rect(sizeA[0], 0, sizeB[0], sizeB[1]));
        this.mat.copyTo(roiA);
        otherImage.mat.copyTo(roiB);
        this.mat.delete();
        this.mat = newMat;
        roiA.delete();
        roiB.delete();
        return this;
    };

    Image.prototype.saveToFile = function (filepath) {
        new jimp_1.default({
            width: this.mat.cols,
            height: this.mat.rows,
            data: Buffer.from(this.mat.data)
        })
            .write(filepath);
    };

    Image.prototype.getWidth = function () {
        return this.getSize()[0];
    };

    Image.prototype.getHeight = function () {
        return this.getSize()[1];
    };

    Image.prototype.getNumberOfChannels = function () {
        return this.getSize()[2];
    };

    Image.prototype.hasAlpha = function () {
        return this.getNumberOfChannels() === 4;
    };

    Image.prototype.toBase64PNG = function () {
        var _a = __read(this.getSize(), 3), width = _a[0], height = _a[1], _ = _a[2];
        var data = this.getBuffer();
        var image = new jimp_1.default({ width: width, height: height, data: data });
        var base64 = (0, Util_1.executeSync)(image.getBase64Async(jimp_1.default.MIME_PNG));
        return base64;
    };

    Image.prototype.show = function (title) {
        if (title === void 0) { title = null; }
        var origChannels = this.mat.channels();
        if (this.mat.channels() === 1) {
            opencv_js_1.cv.cvtColor(this.mat, this.mat, opencv_js_1.cv.COLOR_GRAY2RGB, 0);
        }
        var _a = __read(this.getSize(), 3), width = _a[0], height = _a[1], _ = _a[2];
        var data = this.getBuffer();
        var image = new jimp_1.default({ width: width, height: height, data: data });
        var pngBuffer = (0, Util_1.executeSync)(image.getBufferAsync(jimp_1.default.MIME_PNG));
        var tmpFile = (0, Util_1.writeTempFile)(pngBuffer, title, ".png");
        if (process.platform === "darwin") {
            (0, Util_1.runShellCommand)("open \"" + tmpFile + "\"");
        }
        else if (process.platform === "win32") {
            (0, Util_1.runShellCommandAsync)("" + tmpFile);
        }
        if (origChannels === 1) {
            opencv_js_1.cv.cvtColor(this.mat, this.mat, opencv_js_1.cv.COLOR_RGB2GRAY, 0);
        }
        return this;
    };

    Image.prototype.delete = function () {
        this.mat.delete();
    };

    Image.prototype.getSize = function () {
        var numChannels = this.mat.channels();
        numChannels = numChannels ? numChannels : 1;
        return [this.mat.cols, this.mat.rows, numChannels];
    };

    Image.prototype.getBuffer = function () {
        return Buffer.from(this.mat.data);
    };

    Image.prototype.toCanvas = function () {
        var canvas = (0, canvas_1.createCanvas)(this.getWidth(), this.getHeight());
        var ctx = canvas.getContext('2d');
        var image = (0, Util_1.executeSync)((0, canvas_1.loadImage)(this.toBase64PNG()));
        ctx.drawImage(image, 0, 0);
        return canvas;
    };

    Image.fromCanvas = function (canvas) {
        var dataURL = canvas.toDataURL("image/png");
        return Image.fromBase64(dataURL.substr(22));
    };
    return Image;
}());

exports.Image = Image;
