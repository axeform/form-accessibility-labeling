"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};

var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.Browser = void 0;

var selenium_webdriver_1 = require("selenium-webdriver");
var chrome = require("selenium-webdriver/chrome");
var edge = require("selenium-webdriver/edge");
var deasync = require("deasync");
var Util_1 = require("./Util");
var applescript = require('applescript');
var fs_1 = require("fs");
var bringPIDToFront = require("../lib/bringToFront/index").pid;
selenium_webdriver_1.logging.installConsoleHandler();
var Image_1 = require("./Image");

var Browser = /** @class */ (function () {
    /**
     * @param name one of "safari", "firefox", "chrome", "edge", or "opera"
     */
    function Browser(name, offScreen, headless) {
        var e_1, _a;
        if (name === void 0) { name = "chrome"; }
        if (offScreen === void 0) { offScreen = true; }
        if (headless === void 0) { headless = false; }
        this.name = name;
        this.isHeadless = headless;
        name = name === "edge" ? "MicrosoftEdge" : name;
        
        // The following block of code needs to be kept here, before
        // launching the driver itself. This is because, for Safari, 
        // once the driver has been launched, it does not accept further
        // manual control by end users (or, in this case, Apple script)
        if (this.name.toLowerCase().includes("safari")) {
            var loaded_1 = false;
            applescript.execString("\n        tell application \"Safari\" to activate\n        delay 3       \n        tell application \"System Events\"\n            tell process \"Safari\"\n                tell menu bar 1\n                    tell menu bar item \"Develop\"\n                        tell menu \"Develop\"\n                            click menu item \"Disable Cross-Origin Restrictions\"\n                        end tell\n                    end tell\n                end tell\n            end tell\n        end tell            \n            ", function (err, retval) { loaded_1 = true; });
            deasync.loopWhile(function () { return !loaded_1; });
        }

        var chromeOptions = new chrome.Options().addArguments("--disable-web-security", "--disable-site-isolation-trials", "--hide-scrollbars").windowSize({ width: 1366, height: 768 });
        var edgeOptions = new edge.Options().addArguments("--disable-web-security", "--disable-site-isolation-trials", "--hide-scrollbars").windowSize({ width: 1366, height: 768 });
        if (headless) {
            chromeOptions.headless();
            edgeOptions.headless();
            if (name === "safari") {
                throw new Error("safari does not support headless mode.");
            }
        }

        this.driver = (0, Util_1.executeSync)(new selenium_webdriver_1.Builder().forBrowser(name)
            .setChromeOptions(chromeOptions)
            .setEdgeOptions(edgeOptions)
            .build());
        this.maximizeWindow();
        var size = this.getWindowSize();
        this.screenWidth = size[0];
        this.screenHeight = size[1];
        if (offScreen) {
            this.moveWindowOffscreen();
        }
        try {
            for (var _b = __values(["exit", "SIGINT", "SIGUSR1",
                "SIGUSR2", "uncaughtException", "SIGTERM"]), _c = _b.next(); !_c.done; _c = _b.next()) {
                var event_1 = _c.value;
                process.on(event_1, this.quit);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }

    Browser.prototype.getPageURL = function () {
        return this.executeScript("return document.location.href;");
    };

    Browser.prototype.getName = function () {
        return this.name;
    };

    Browser.prototype.goto = function (url, timeoutSeconds) {
        if (timeoutSeconds === void 0) { timeoutSeconds = 30; }
        (0, Util_1.executeSync)(this.driver.get(url), timeoutSeconds);
    };

    Browser.prototype.getWindowClientSize = function () {
        return this.executeScript("return [document.documentElement.clientWidth, document.documentElement.clientHeight]");
    };

    Browser.prototype.maximizeWindow = function () {
        (0, Util_1.executeSync)(this.driver.manage().window().maximize());
    };

    Browser.prototype.resetWindowSize = function () {
        this.setWindowSize(this.screenWidth, this.screenHeight);
    };

    Browser.prototype.moveWindowOffscreen = function () {
        this.maximizeWindow();
        var rect = (0, Util_1.executeSync)(this.driver.manage().window().getRect());
        (0, Util_1.executeSync)(this.driver.manage().window().setRect({ x: rect.width - 1, y: rect.height - 1, width: rect.width, height: rect.height }));
        if (this.name.toLowerCase().includes("safari")) {
            var loaded_2 = false;
            applescript.execString("\n        tell application \"System Events\" to tell process \"Safari\"\n            set position of window 1 to {" + (rect.width - 1) + ", " + (rect.height - 1) + "}\n            set position of window 2 to {" + (rect.width - 1) + ", " + (rect.height - 1) + "}\n        end tell            \n            ", function (err, retval) { loaded_2 = true; });
            deasync.loopWhile(function () { return !loaded_2; });
        }
    };

    Browser.prototype.setWindowSize = function (width, height) {
        try {
            var rect = (0, Util_1.executeSync)(this.driver.manage().window().getRect());
            (0, Util_1.executeSync)(this.driver.manage().window().setRect({ x: rect.x, y: rect.y, width: width, height: height }));
        }
        catch (err) {
            console.error(err);
            console.error("width = " + width + ",   height = " + height);
        }
    };

    Browser.prototype.setWindowPosition = function (x, y) {
        var rect = this.getWindowRect();
        this.setWindowRect(x, y, rect[2], rect[3]);
    };

    Browser.prototype.setWindowRect = function (x, y, width, height) {
        (0, Util_1.executeSync)(this.driver.manage().window().setRect({ x: x, y: y, width: width, height: height }));
    };

    Browser.prototype.getWindowRect = function () {
        var rect = (0, Util_1.executeSync)(this.driver.manage().window().getRect());
        return [rect.x, rect.y, rect.width, rect.height];
    };

    Browser.prototype.getWindowSize = function () {
        var rect = (0, Util_1.executeSync)(this.driver.manage().window().getRect());
        return [rect.width, rect.height];
    };

    Browser.prototype.resetFocus = function () {
        this.executeScript("\n        document.activeElement.blur();\n        document.body.focus();\n        ");
    };

    Browser.prototype.takeScreenshot = function (fitToContent) {
        if (fitToContent === void 0) { fitToContent = false; }
        this.bringWindowToFront();
        this.resetFocus();
        if (fitToContent) {
            this.fitWindowToContent__2();
        }
        var b64 = (0, Util_1.executeSync)(this.driver.takeScreenshot());
        return Image_1.Image.fromBase64(b64);
    };

    Browser.prototype.getContentHeight = function () {
        var elementsBoxes = this.executeScript("\n        return Array.from(document.querySelectorAll(\"*\"))\n                .map(element => element.getBoundingClientRect())\n                .filter(rect => rect.width*rect.height>=100 && rect.x>=0 && rect.y>=0)");
        var contentHeight = Math.ceil(elementsBoxes.sort(function (r1, r2) { return r2.bottom - r1.bottom; })[0].bottom);
        return contentHeight;
    };

    Browser.prototype.scrollAllContent = function () {
        console.log("content height before scroll = " + this.getContentHeight());
        console.log("window rect before scroll = " + this.getWindowRect());
        var scrollDelta = Math.floor(this.screenHeight / 3);
        var shouldTerminate = false, iteration = 0;
        var currentScrollPosition, lastScrollPosition = 0;
        var originalSize = this.getWindowSize();
        this.resetWindowSize();
        while (!shouldTerminate) {
            this.executeScript("scrollBy(0, " + scrollDelta + ");");
            this.sleep(100);
            currentScrollPosition = this.executeScript("return document.documentElement.scrollTop");
            if (currentScrollPosition === lastScrollPosition) {
                shouldTerminate = true;
            }
            else {
                lastScrollPosition = currentScrollPosition;
            }
            if (iteration > 100) {
                shouldTerminate = true;
            }
            iteration++;
        }
        console.log("# iterations = " + iteration);
        var newContentHeight = this.getContentHeight();
        this.setWindowSize(originalSize[0], currentScrollPosition + this.screenHeight + 300);
        // this.fitWindowToContent();
        this.sleep(500);
        this.executeScript("scrollBy(0, -1000000);");
        this.sleep(500);
        console.log("window rect after scroll = " + this.getWindowRect());
        // console.log(`content height after scroll = ${this.getContentHeight()}`)
    };

    Browser.prototype.bringWindowToFront = function () {
        if (this.isHeadless)
            return;
        var browserName = this.name.toLowerCase();
        var applicationName;
        if (browserName.includes("safari")) {
            applicationName = "Safari";
        }
        if (browserName.includes("chrome")) {
            applicationName = "Google Chrome";
        }
        if (browserName.includes("firefox")) {
            applicationName = "Firefox";
        }
        if (browserName.includes("edge")) {
            applicationName = "Microsoft Edge";
        }
        if (browserName.includes("opera")) {
            applicationName = "Opera";
        }
        if (process.platform === "darwin") {
            applescript.execString("tell application \"" + applicationName + "\" to activate");
        }
        else if (process.platform === "win32") {
            var docTitle = this.executeScript("return document.title;");
            var pids = Util_1.Win32.FindPIDsByWindowTitle(docTitle);
            bringPIDToFront(pids[0]);
        }
    };

    Browser.prototype.fitWindowToContent__2 = function (delay) {
        if (delay === void 0) { delay = 10; }
        var loadDelay = delay; // default: 2000
        this.resetWindowSize();
        this.bringWindowToFront();
        var browserChromeHeightOffset = this.screenHeight - this.getWindowClientSize()[1];
        var scrollDelta = Math.floor(this.screenHeight / 3);
        var shouldTerminate = false, contentHasBeenFit = false;
        var currentScrollPosition, lastScrollPosition = 0, iteration = 0;
        var stableScrollCounter = 0;
        var originalSize = this.getWindowSize();
        this.resetWindowSize();
        // this.setWindowPosition(0, 80);
        // this.setWindowPosition(0,0);
        while (!shouldTerminate) {
            // this.executeScript(`scrollBy(0, ${scrollDelta});`)
            this.scrollBy(0, scrollDelta);
            this.sleep(loadDelay);
            currentScrollPosition = this.executeScript("return document.documentElement.scrollTop");
            if (currentScrollPosition === lastScrollPosition) {
                stableScrollCounter++;
            }
            else {
                lastScrollPosition = currentScrollPosition;
                stableScrollCounter = 0;
            }
            if (stableScrollCounter > 5) {
                shouldTerminate = true;
                contentHasBeenFit = true;
            }
            if (lastScrollPosition > this.screenWidth * 10) {
                shouldTerminate = true;
            }
            if (iteration > 100) {
                shouldTerminate = true;
            }
            iteration++;
        }
        // console.log(`
        // fitWindowToContent__2:
        // # iterations = ${iteration}
        // lastScrollPosition = ${lastScrollPosition}
        // currentScrollPosition = ${currentScrollPosition}
        // contentHasBeenFit = ${contentHasBeenFit}
        // `);
        this.executeScript("scrollBy(0, -1000000);");
        this.setWindowSize(originalSize[0], lastScrollPosition + this.screenHeight + browserChromeHeightOffset);
        this.sleep(loadDelay);
        return contentHasBeenFit;
    };

    Browser.prototype.fitWindowToContent = function () {
        var windowRect = this.getWindowRect();
        if (windowRect[1] < (this.screenHeight / 2)) {
            this.maximizeWindow();
            this.moveWindowOffscreen();
        }
        var browserChromeHeightOffset = this.screenHeight - this.getWindowClientSize()[1];
        var contentHeight, windowClientHeight;
        var contentHasBeenFit = false, shouldTerminate = false, iteration = 0;
        var heightDiff_current, heightDiff_prev = Number.POSITIVE_INFINITY;
        while (!shouldTerminate) {
            contentHeight = this.getContentHeight();
            windowClientHeight = this.getWindowSize()[1] - browserChromeHeightOffset;
            if (windowClientHeight >= contentHeight) {
                contentHasBeenFit = true;
                shouldTerminate = true;
            }
            else {
                heightDiff_current = contentHeight - windowClientHeight;
                if (heightDiff_current >= heightDiff_prev) {
                    shouldTerminate = true;
                }
                else {
                    var _a = __read(this.getWindowSize(), 2), width = _a[0], _1 = _a[1];
                    this.setWindowSize(width, windowClientHeight + browserChromeHeightOffset + (contentHeight - windowClientHeight));
                    heightDiff_prev = heightDiff_current;
                }
            }
            if (iteration > 5) {
                shouldTerminate = true;
            }
            console.log("\n    iteration: " + iteration + "\n    contentHeight: " + contentHeight + "\n    windowHeight: " + windowClientHeight + "\n    contentHasBeenFit: " + contentHasBeenFit + "        \n            ");
            iteration++;
        }
        return contentHasBeenFit;
    };

    Browser.prototype.waitForLoading = function (timeoutSeconds) {
        // let t0 = Date.now();
        // let timestamp: number;
        // while (Date.now() - t0 < timeoutSeconds*1000) {
        //     timestamp = executeSync(this.driver.executeScript(`return performance.timing.loadEventEnd`));
        //     if (timestamp>0) break;
        //     this.sleep(300);
        // }
        if (timeoutSeconds === void 0) { timeoutSeconds = 5; }
        // // check if all frames have been loaded
        // let allFramesHaveBeenLoaded: boolean, allImagesHaveBeenLoaded: boolean;
        // t0 = Date.now();
        // while (Date.now() - t0 < timeoutSeconds*1000) {
        //     allFramesHaveBeenLoaded = executeSync(this.driver.executeScript(`return Array.from(document.querySelectorAll("iframe"))
        //     .map(iframe => iframe.contentDocument)
        //     .filter(document => document!==null)
        //     .map(document => document.readyState)
        //     .every(readyState => readyState==="complete");`))
        //     allImagesHaveBeenLoaded = executeSync(this.driver.executeScript(`
        //     return [...Array.from(document.querySelectorAll("img")), ...Array.from(document.querySelectorAll("iframe")).map(iframe=>iframe.contentDocument).filter(document=>document!==null).map(document=>Array.from(document.querySelectorAll("img"))).filter(x=>x instanceof HTMLImageElement)].map(img=>img.complete).every(status=>status===true)        
        //     `))
        //     if (allFramesHaveBeenLoaded && allImagesHaveBeenLoaded) {
        //         break;
        //     } else {
        //         this.sleep(300);
        //     }
        // }        
        // if (timestamp>0 && allFramesHaveBeenLoaded && allImagesHaveBeenLoaded) {
        //     return;
        // } else {
        //     throw new Error("timeout");
        // }
        this.sleep(5000);
    };

    Browser.prototype.takeScrollshot = function () {
        this.bringWindowToFront();
        this.resetFocus();
        this.scrollBy(0, -1000000);
        this.waitForLoading();
        var screenshot = this.takeScreenshot();
        var scrollPositions = [], N;
        do {
            scrollPositions.push((0, Util_1.executeSync)(this.driver.executeScript("return window.scrollY;")));
            this.scrollBy(0, 500);
            this.waitForLoading();
            scrollPositions.push((0, Util_1.executeSync)(this.driver.executeScript("return window.scrollY;")));
            N = scrollPositions.length;
            if (scrollPositions[N - 1] === scrollPositions[N - 2]) {
                break;
            }
            else {
                screenshot.appendBelow(this.takeScreenshot());
            }
        } while (scrollPositions[scrollPositions.length - 1] < 10000);
        return screenshot;
    };

    Browser.prototype.scrollBy = function (x, y) {
        (0, Util_1.executeSync)(this.driver.executeScript("scrollBy(" + x + "," + y + ");"));
    };

    Browser.prototype.sleep = function (milliseconds) {
        (0, Util_1.executeSync)(this.driver.sleep(milliseconds));
    };

    Browser.prototype.quit = function () {
        try {
            this.driver.quit();
            var browserName = this.name.toLowerCase();
            var applicationName = void 0;
            if (browserName.includes("safari")) {
                applicationName = "Safari";
            }
            if (browserName.includes("chrome")) {
                applicationName = "Google Chrome";
            }
            if (browserName.includes("firefox")) {
                applicationName = "Firefox";
            }
            if (browserName.includes("edge")) {
                applicationName = "Microsoft Edge";
            }
            if (browserName.includes("opera")) {
                applicationName = "Opera";
            }
            if (process.platform === "darwin") {
                applescript.execString("tell application \"" + applicationName + "\" to quit");
            }
        }
        catch (err) { }
    };

    Browser.prototype.executeScript = function (script) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (args) {
            return (0, Util_1.executeSync)(this.driver.executeScript(script, args));
        }
        else {
            return (0, Util_1.executeSync)(this.driver.executeScript(script));
        }
    };
    
    Browser.prototype.loadScriptFromFile = function (filepath) {
        this.executeScript((0, fs_1.readFileSync)(filepath, { encoding: 'utf8', flag: 'r' }));
    };
    return Browser;
}());
exports.Browser = Browser;
