**Objective**
This tool analyzes a given web page and generates accessibility labels (if any) for web forms, in order to make the 
form accessible for users with disabilities.

**Known limitations (so far)**

 - Unable to process captcha elements in forms
 - Reduced labeling quality in very dense forms

**Installation**

 1. Run `npm install`.  Only if it doesn't work, try `npm ci`. Please note that the installation takes time. Expect 5 - 10 minutes depending on your computer. 
 3. Change the `inputURL` in `Test.js`
 4. Run `node Test.js`

