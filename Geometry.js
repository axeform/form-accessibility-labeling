"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};

var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};

var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};

Object.defineProperty(exports, "__esModule", { value: true });
exports.Rectangle = exports.Point = exports.SpatialIndex = void 0;
var core_1 = __importDefault(require("@flatten-js/core"));
var flatbush_1 = __importDefault(require("flatbush"));
var Util_1 = require("./Util");

var SpatialIndex = /** @class */ (function () {
    function SpatialIndex(rectangleArray) {
        var e_1, _a;
        this.index = new flatbush_1.default(rectangleArray.length);
        try {
            for (var rectangleArray_1 = __values(rectangleArray), rectangleArray_1_1 = rectangleArray_1.next(); !rectangleArray_1_1.done; rectangleArray_1_1 = rectangleArray_1.next()) {
                var rect = rectangleArray_1_1.value;
                this.index.add(rect.left, rect.top, rect.right, rect.bottom);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (rectangleArray_1_1 && !rectangleArray_1_1.done && (_a = rectangleArray_1.return)) _a.call(rectangleArray_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.index.finish();
        this.rectangles = rectangleArray;
    }
    
    SpatialIndex.prototype.search = function (queryRectangle) {
        var _this = this;
        var left = queryRectangle.left, top = queryRectangle.top, right = queryRectangle.right, bottom = queryRectangle.bottom;
        return this.index.search(left, top, right, bottom).map(function (i) { return _this.rectangles[i]; });
    };
    
    SpatialIndex.prototype.searchY = function (topCoordinate, bottomCoordinate) {
        var query = new Rectangle(this.index.minX, topCoordinate, this.index.maxX, bottomCoordinate);
        return this.search(query);
    };
    
    SpatialIndex.prototype.searchX = function (leftCoordinate, rightCoordinate) {
        var query = new Rectangle(leftCoordinate, this.index.minY, rightCoordinate, this.index.maxY);
        return this.search(query);
    };
    
    SpatialIndex.prototype.getNeighbors = function (queryRectangle, maxResults, maxDistance) {
        var _this = this;
        if (maxResults === void 0) { maxResults = Infinity; }
        if (maxDistance === void 0) { maxDistance = Infinity; }
        var _a = __read(queryRectangle.getCenter(), 2), x = _a[0], y = _a[1];
        return this.index.neighbors(x, y, maxResults, maxDistance).map(function (i) { return _this.rectangles[i]; });
    };
    
    return SpatialIndex;
}());
exports.SpatialIndex = SpatialIndex;
var Point = /** @class */ (function () {
    function Point(coords) {
        this.x = coords[0];
        this.y = coords[1];
    }
    
    Point.prototype.getDistanceTo = function (anotherPoint) {
        var dx = this.x - anotherPoint.x;
        var dy = this.y - anotherPoint.y;
        return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    };

    Point.prototype.asArray = function () {
        return [this.x, this.y];
    };
    return Point;
}());
exports.Point = Point;
var Rectangle = /** @class */ (function () {
    
    function Rectangle(left, top, right, bottom) {
        this.left = Math.round(left);
        this.top = Math.round(top);
        this.right = Math.round(right);
        this.bottom = Math.round(bottom);
        this.box = new core_1.default.Box(this.left, this.top, this.right, this.bottom);
    }
    
    Rectangle.prototype.hashCode = function () {
        return (0, Util_1.getHashOf)(this.toString());
    };
    
    Rectangle.prototype.toString = function () {
        return [this.left, this.top, this.right, this.bottom].join(",");
    };
    
    Rectangle.prototype.equals = function (other) {
        if (other) {
            return (this.left === other.left && this.top === other.top
                && this.right === other.right && this.bottom === other.bottom);
        }
        else {
            return false;
        }
    };
    
    Rectangle.fromDOMRect = function (domRect) {
        var rect = new Rectangle(domRect.left, domRect.top, domRect.right, domRect.bottom);
        return rect;
    };
    
    Rectangle.ofElement = function (xpath, browser) {
        var xpathString = xpath.toString();
        var domRect = browser.executeScript("\n            const element = document.evaluate(\"" + xpathString + "\", document, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null).singleNodeValue;\n            return element.getBoundingClientRect().toJSON();\n        ");
        return Rectangle.fromDOMRect(domRect);
    };
    
    Rectangle.fromMergingOf = function (rectangles) {
        var leftCoords = rectangles.map(function (rect) { return rect.left; });
        var topCoords = rectangles.map(function (rect) { return rect.top; });
        var rightCoords = rectangles.map(function (rect) { return rect.right; });
        var bottomCoords = rectangles.map(function (rect) { return rect.bottom; });
        return new Rectangle(Math.min.apply(Math, __spreadArray([], __read(leftCoords), false)), Math.min.apply(Math, __spreadArray([], __read(topCoords), false)), Math.max.apply(Math, __spreadArray([], __read(rightCoords), false)), Math.max.apply(Math, __spreadArray([], __read(bottomCoords), false)));
    };
    
    Rectangle.prototype.clone = function () {
        return new Rectangle(this.left, this.top, this.right, this.bottom);
    };
    
    Rectangle.prototype.getArea = function () {
        // return geometric.polygonArea(this._toPolygon());
        return (this.right - this.left) * (this.bottom - this.top);
    };
    
    Rectangle.prototype.getCenter = function () {
        var width = Math.abs(this.right - this.left);
        var height = Math.abs(this.bottom - this.top);
        return [Math.round(this.left + (width / 2)), Math.round(this.top + (height / 2))];
        // return geometric.polygonCentroid(this._toPolygon());
    };
    
    Rectangle.prototype.getWidth = function () {
        return this.right - this.left;
    };
    
    Rectangle.prototype.getHeight = function () {
        return this.bottom - this.top;
    };
    
    Rectangle.prototype.getCenterDistanceTo = function (otherRectangle) {
        var _a = __read(this.getCenter(), 2), x1 = _a[0], y1 = _a[1];
        var _b = __read(otherRectangle.getCenter(), 2), x2 = _b[0], y2 = _b[1];
        return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    };
    
    Rectangle.prototype.getRelationTo = function (otherRectangle) {
        var relation = "";
        if (this.left >= otherRectangle.right) {
            relation = "right,";
        }
        if (this.right <= otherRectangle.left) {
            relation = "left,";
        }
        if (this.top >= otherRectangle.bottom) {
            relation += "bottom";
        }
        if (this.bottom <= otherRectangle.top) {
            relation += "top";
        }
        return relation;
    };
    
    Rectangle.prototype.getMinimumDistanceTo = function (otherRectangle) {
        var abs = Math.abs;
        var deltaHorizontal = Math.min(abs(this.left - otherRectangle.left), abs(this.left - otherRectangle.right), abs(this.right - otherRectangle.right), abs(this.right - otherRectangle.left));
        var deltaVertical = Math.min(abs(this.top - otherRectangle.top), abs(this.top - otherRectangle.bottom), abs(this.bottom - otherRectangle.bottom), abs(this.bottom - otherRectangle.top));
        return Math.sqrt(Math.pow(deltaHorizontal, 2) + Math.pow(deltaVertical, 2));
    };
    
    Rectangle.prototype.getMinimumVerticalDistanceTo = function (otherRectangle) {
        var abs = Math.abs;
        var deltaVertical = Math.min(abs(this.top - otherRectangle.top), abs(this.top - otherRectangle.bottom), abs(this.bottom - otherRectangle.bottom), abs(this.bottom - otherRectangle.top));
        return deltaVertical;
    };
    
    Rectangle.prototype.intersectsWith = function (otherRectangle) {
        // return geometric.polygonIntersectsPolygon(this._toPolygon(), otherRectangle._toPolygon());
        return this.box.intersect(otherRectangle.box);
    };

    Rectangle.prototype.contains = function (otherRectangle) {
        var result = (otherRectangle.left >= this.left
            && otherRectangle.right <= this.right
            && otherRectangle.top >= this.top
            && otherRectangle.bottom <= this.bottom);
        // console.log("\n\n")
        // console.log(this);
        // console.log(otherRectangle);
        // console.log(result)
        return result;
        // return geometric.polygonInPolygon(otherRectangle._toPolygon(), this._toPolygon());
    };

    Rectangle.prototype.overlapsWith = function (otherRectangle) {
        return this.intersectsWith(otherRectangle) || this.contains(otherRectangle) || otherRectangle.contains(this);
    };
    
    Rectangle.prototype.expandBy = function (numberOfPixels) {
        this.left = this.left - numberOfPixels;
        this.top = this.top - numberOfPixels;
        this.right = this.right + numberOfPixels;
        this.bottom = this.bottom + numberOfPixels;
        return this;
    };
    //     let scaled = geometric.polygonScaleX(this._toPolygon(), factorX);
    //     scaled = geometric.polygonScaleY(scaled, factorY);
    //     this.left = scaled[0][0];
    //     this.top = scaled[0][1];
    //     this.right = scaled[1][0];
    //     this.bottom = scaled[2][1];
    // }

    Rectangle.prototype._toPolygon = function () {
        // const rectangle = [[0, 0], [0, 1], [1, 1], [1, 0]]; // from docs
        return [[this.left, this.top], [this.right, this.top],
            [this.right, this.bottom], [this.left, this.bottom]];
    };
    return Rectangle;
}());
exports.Rectangle = Rectangle;
